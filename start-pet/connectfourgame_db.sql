--
-- PostgreSQL database dump
--

-- Dumped from database version 11.19
-- Dumped by pg_dump version 15rc2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accounts (
    "idUser" integer NOT NULL,
    login character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    role character varying(255)
);


ALTER TABLE public.accounts OWNER TO postgres;

--
-- Name: counters; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.counters (
    id integer NOT NULL,
    user_id integer NOT NULL,
    symbol character(1) NOT NULL,
    month timestamp without time zone DEFAULT date_trunc('month'::text, now()) NOT NULL,
    wins_cnt integer DEFAULT 0 NOT NULL,
    loses_cnt integer DEFAULT 0 NOT NULL,
    draws_cnt integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.counters OWNER TO postgres;

--
-- Name: counters_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.counters_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.counters_id_seq OWNER TO postgres;

--
-- Name: counters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.counters_id_seq OWNED BY public.counters.id;


--
-- Name: games; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.games (
    id integer NOT NULL,
    "idPlayer1" integer NOT NULL,
    "idPlayer2" integer NOT NULL,
    "gameResult" boolean,
    "gameBegin" timestamp without time zone NOT NULL,
    "gameEnd" timestamp without time zone,
    "winField" jsonb
);


ALTER TABLE public.games OWNER TO postgres;

--
-- Name: games_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.games_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_id_seq OWNER TO postgres;

--
-- Name: games_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.games_id_seq OWNED BY public.games.id;


--
-- Name: knex_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.knex_migrations (
    id integer NOT NULL,
    name character varying(255),
    batch integer,
    migration_time timestamp with time zone
);


ALTER TABLE public.knex_migrations OWNER TO postgres;

--
-- Name: knex_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.knex_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.knex_migrations_id_seq OWNER TO postgres;

--
-- Name: knex_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.knex_migrations_id_seq OWNED BY public.knex_migrations.id;


--
-- Name: knex_migrations_lock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.knex_migrations_lock (
    index integer NOT NULL,
    is_locked integer
);


ALTER TABLE public.knex_migrations_lock OWNER TO postgres;

--
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.knex_migrations_lock_index_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.knex_migrations_lock_index_seq OWNER TO postgres;

--
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.knex_migrations_lock_index_seq OWNED BY public.knex_migrations_lock.index;


--
-- Name: messenger; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messenger (
    "idGame" integer NOT NULL,
    "idUser" integer NOT NULL,
    message character varying(255) NOT NULL,
    role character varying(255) NOT NULL,
    "createdMesAt" timestamp without time zone NOT NULL
);


ALTER TABLE public.messenger OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    "firstName" character varying(255) NOT NULL,
    "surName" character varying(255) NOT NULL,
    "secondName" character varying(255) NOT NULL,
    age integer NOT NULL,
    gender boolean NOT NULL,
    "statusActive" boolean NOT NULL,
    "createdAt" timestamp without time zone NOT NULL,
    "updatedAt" timestamp without time zone NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: counters id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.counters ALTER COLUMN id SET DEFAULT nextval('public.counters_id_seq'::regclass);


--
-- Name: games id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games ALTER COLUMN id SET DEFAULT nextval('public.games_id_seq'::regclass);


--
-- Name: knex_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations ALTER COLUMN id SET DEFAULT nextval('public.knex_migrations_id_seq'::regclass);


--
-- Name: knex_migrations_lock index; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations_lock ALTER COLUMN index SET DEFAULT nextval('public.knex_migrations_lock_index_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.accounts VALUES
	(2, 'log30', '$2b$05$SyWOI/vugslVSltUKdDKx.pnZQeSFaj4bIKTYlJzf8JJcnqEYss5e', 'user'),
	(3, 'log40', '$2b$05$YeOla11o4eP3kbx9mgElgOjflndcpfFkePBAWGdPQQc9SS5UD9Kwu', 'user'),
	(4, 'log50', '$2b$05$d2ME5WRJINByrE8k9cvZjOUJYFI4ruSmp25FDiaW2u0XrpzXUr9ay', 'user'),
	(6, 'log150', '$2b$05$zPcN0VcP.52HH5AbMlc19uVIHhLC4qi3KvWaN2kP8amQ0WBZ2ZUXq', 'user'),
	(5, 'log60', '$2b$05$W9a4SuAiTYHS9e0TZPh0nuOZpXEE/mJXusOmycc4pHJIZk5yrOnPa', 'admin'),
	(8, 'log151', '$2b$05$gMWfnb6uM6wDbmQFm2efBu3.ou7rRZhs4K4/BYo8rmwHTGBwWrq1q', 'user'),
	(9, 'log152', '$2b$05$qbzbsCXqQZt7i1PxN1lDzuIn/knKG38/kXwHirk7Dn39FN5Vyxt5u', 'user'),
	(10, 'log153', '$2b$05$4ks2zi6vy8SDabefmkz.Fu.W/Lu0V/fK97cJ1srta9Je7CiI/T29e', 'user'),
	(11, 'log154', '$2b$05$oiP.RlIiUXGTxvyXucuV7.Ekw641qk/tv52lc92MzvjBEOaRkjjUS', 'user'),
	(12, 'log155', '$2b$05$3ar.e7lwm02r9CqTVUS5/eTDkBhlDc4M3fglGyzFF.Ra.VARyg41y', 'user'),
	(13, 'log156', '$2b$05$cThO4rCruUJgTgPjGO3sgO3tqIMyZ6vKFfTYwgMeIDVyiB0KcwFUK', 'user'),
	(14, 'log157', '$2b$05$Ds7dqS3kE5eWJ6pfJw/z8ug9p4T0o.PeIgiuukFpc3XRlE625ZQku', 'user'),
	(15, 'log158', '$2b$05$vNoRP/DJQj3roCA8o3AVYeB70aEyngzETrj2bsSdMVt5Fb3yMkE2W', 'user'),
	(16, 'log159', '$2b$05$tHUDFnHI13ru.MuhSpOl6OVu7663ZLYcxUrUxguf9I7FfeMZPWySa', 'user'),
	(17, 'log160', '$2b$05$aBmChiVrQ2O931Lg1yPmAeqmvFgIkR4fg9EnnDb86UF2Rwa1.wcSu', 'user'),
	(18, 'log161', '$2b$05$Q.hd9RiyHZotWpMlSrEAou2i898dfGjS2vFbg7Z18m0JQRGCP3PiC', 'user'),
	(19, 'log162', '$2b$05$3j/3IbOjG3igAXMcdlwqxOF8ZLdJ6CZNVyXxW1Gv3repFwYSV.1l2', 'user'),
	(20, 'log163', '$2b$05$Rwz3ol0yy/psWKFlu/qxzeYcz1F/l7y1FeOEy/j9bxsIPgIp7pFE6', 'user'),
	(21, 'log164', '$2b$05$ob0WBBfcL23BSHCTH8NBcupmcAksPbCp5z5KFkKk8LTOhhIAmYZXO', 'user'),
	(22, 'log165', '$2b$05$79qTFdjhMNAyjHYzLIqTU.c03FS95VleIVu4vnU5QTGnLVeCJnvqK', 'user'),
	(23, 'log166', '$2b$05$4Wp/rGjiWGoYtjJHM6pIVuzyUI5yUEBYhhqbHXDdj3Vh7kOTcZuc.', 'user'),
	(24, 'log168', '$2b$05$AY6F98hUkbPJZuCPka/rjuVDlhs3a3FSjt8Cm8Raw6QAKagpX12fC', 'user'),
	(25, 'log1231', '$2b$05$kDhjUKDMnKQ4KXdSwJ.5nuj9QBxU/F2Ot9EY.QzHBXaPB2anmRBW.', 'user'),
	(26, 'log1232', '$2b$05$8chQ4FLSAqEifTvz0McOF.B2Y.ZCOJYZmOa/pIBDT9e5M./KPz.qy', 'user'),
	(27, 'log1234235', '$2b$05$znmLNAAoBiBtK5zWRvcXJOJxUdQ.3f9uBtUK6WRj7gNb8FT5.030O', 'user'),
	(28, 'log1234543', '$2b$05$RLYEEO27oXEsVDlxXqf8s.4j6DtGHaPp9psnE7t2KT9bnMwjFiN8y', 'user'),
	(29, 'log123454324', '$2b$05$jlH21DroeYX60w.aU.MR7O/7odg51QsiqAuimVn83Lj8s5jW7puC.', 'user'),
	(30, 'log1234543244', '$2b$05$BbcoEb2Cq8..0MuQE2IFkeCfYrD.zud2kONv4QKGxC6Wc8M/e6b5a', 'user'),
	(31, 'log12345432443', '$2b$05$kKVSU/0r5cP1aWXqAZt7n.8STDp3zijFBM4KDukpbkfSCbwhZyytK', 'user'),
	(32, 'log123454324433', '$2b$05$ke0svtKaGNT7CUdNbiFyZ.Sfdg0IQ/7CvDSrwAwf02apIC50k42.q', 'user'),
	(33, 'log1234543244333', '$2b$05$cKTgc2D3ctTYf/RwAH/D5evOW00aU7l50oVJm/85B5oT9NX6NF.uG', 'user'),
	(34, 'log12345432443333', '$2b$05$48i1cvR2QSbR/kBmGXGJl.Lz0jkGUtkx50db5.x0PVMZ/RcvaxsOy', 'user'),
	(35, 'log123454324433333', '$2b$05$6i5745W7uDkEIwTxqF9dxe8cf/6/YbWQHfFMNfgR/vQX3Mb1.qvg6', 'user'),
	(36, 'log1234543244333333', '$2b$05$P4EPDXyjuhCnFM8S05kgA.iKLbPDbm6PFYOrp4uoFwUy.2w3eCpBK', 'user'),
	(37, 'log12345432443333333', '$2b$05$yafm1p7bswT/2EFrk.pBtetwJkuWewETWFapoAqHltA1KNkDR.Rn6', 'user'),
	(38, 'log123454324433333333', '$2b$05$WWNBFu595YeH1sdmK2rHu.I2dw3HYmEvHZYKX4t9Zy/pIMvqD/ABG', 'user'),
	(39, 'log1234543244333333334', '$2b$05$LHZFIHOVi0CQdto9hyjxS.BbEwx00OBg2BRhtPaL/B0CJiacR.ZmK', 'user'),
	(40, 'log12345432443333333343', '$2b$05$86frL7Pmo5..UBnyBxn/XOZWnA4wV2VikdVXLYicFk8a2U4Yjh4Gy', 'user'),
	(41, 'log1', '$2b$05$SLjy663JEAASzfILMWEXLestdY164NjdXbpKAZnAE86UrEgzdUl.S', 'user'),
	(42, 'log11', '$2b$05$rDyPxBXEXtpDv5MUD2AY/ejuPWX52aI5xOio4AgMlGhSySnBsDXmi', 'user'),
	(43, 'log12', '$2b$05$JqLR05p9yU/F0AWzpAfDROQ7Zbffg4uO1lypL0W6PusDoYUfVqMm.', 'user'),
	(44, 'log13', '$2b$05$Zd6JjbucjWo4JKefCFUmCONW7Sffhn0gY2BsE/AwbcsFEq7UxNkFm', 'user'),
	(45, 'log14', '$2b$05$0AbN7aFJc1bn48qx26raYeYJQlu009MwbX3.2FTUjuH5F9bRQWvui', 'user'),
	(46, 'log15', '$2b$05$SYrZHsjp8f8YqrWnOwLvTO.jKIJ2uF5HbCnRGGWbpA7mtZiECajsq', 'user'),
	(47, 'log16', '$2b$05$JZ4adcna2/75138NeH4ID.pC0eIdYNCTvNKU69KWqrcPHtWtcxAcy', 'user'),
	(48, 'log1622', '$2b$05$.aaCDLMv7L78/yI2ZX3D0uQ6DNqHk4xIUTE42kF2aF9eHrYbMOx1u', 'user'),
	(49, 'log16222', '$2b$05$M7Yi2FbKT6uqj4A3to.sNOuYWv1N1HL2ag2TsAeIt5tYcrtrhaQ4m', 'user'),
	(50, 'log162222', '$2b$05$AYId1zs8ghzZZpqxAoHi6e6lWlMjzgcPlHHpA4M915IcL/CApjwkm', 'user'),
	(51, 'log1622222', '$2b$05$Ymoe0SCaYp5JI0qNAHSDcOvbmGIzUvHdptl.m1Xy4ZqHBork009DG', 'user'),
	(52, 'log16222222', '$2b$05$efmUOvQBMPAtzRZmes758ejZCyN2Z77aPrR7h7IAEcoDHm/FBI/o2', 'user'),
	(53, 'log162222222', '$2b$05$xL/5WihhJMmEBd3.T7VojeBFEGu9zul3PE5h57tt1iU7cExwVGLTS', 'user'),
	(54, 'log1622222222', '$2b$05$U81kSbI6X3GknXgrZx7L6uPMT4mHj0W16rDmfzC7gEpU5KAi80G7a', 'user'),
	(55, 'log1622222322', '$2b$05$bZWzPTGb4hoAJLmxA7vI..mMY8GRVIyDivOokN8nxrsAxz4AW0eQ.', 'user'),
	(56, 'log16222223322', '$2b$05$7Wz65f4V6K4c30c.IHhBEuuwbNccJtqCwPzvJKkXHRdJLP8xeNUIq', 'user'),
	(57, 'log162222233322', '$2b$05$zJqF72h6qChz18vcbs.aHuIgo9cX8JxvrZvbHsXzc57uc.u21Cqj6', 'user'),
	(58, 'log1622222333322', '$2b$05$UvRtVGOz9RkdvJb7sVyfDO5qyiLWmpyRYxiZRmKF1qyfRhKorx6rS', 'user'),
	(59, 'log16222223333322', '$2b$05$YoFW91wxPzEnAcsHSlea.OdmqIBIJMPTKQscfe5wjJvFRYHZ9tehK', 'user'),
	(60, 'log162222233333322', '$2b$05$72wNbvjpgw1cQx/rminS0uVU8QXQnLxHlZfiM8FiwprMmvBjDTehK', 'user'),
	(61, 'log164422', '$2b$05$TJlyxC1hEEdwmHP6OMCCjOMve45ruDCFxBDxGpTPirwPq.N9qVgny', 'user'),
	(62, 'log1644422', '$2b$05$jrGZGqFAw.yfZVbGEInj0uY0VQSG6lj5v.AwBTAJvWAxyS5SdIwaO', 'user'),
	(63, 'log16444422', '$2b$05$oisXznYqsQVKMcvDv4Nx8ezMXglLJl09K./.CQBs7YvyXbtWOMspa', 'user'),
	(64, 'log164444422', '$2b$05$ap13lb5j2r1eIAerbx5jVOGAhFTdulxYUoOpWPDGfwIBwnLGijF/C', 'user'),
	(65, 'log1644444422', '$2b$05$QWqT2spD49Tl1oCU3DhcT.gHVNI1owKg.tOy1fOHuC4M7FtNhZCh6', 'user'),
	(66, 'log16444444422', '$2b$05$jODcjkDqe9E8/LDBXIehjOhbATV/UNQ6Ve1gk4JPrwsJ9T/58ZVlq', 'user'),
	(67, 'log122312351231', '$2b$05$kEmvdiN2YLI532.xqS.Xe.BSieR5HJ5koFcV07p1gU17suv6bblj6', 'user'),
	(68, 'dfsd', '$2b$05$8t.T9sB1bTgTfU759OyTUuWQbQ23N0Gj0wqqUgXkixPgnoHrQLIm2', 'user'),
	(69, 'gub', '$2b$05$p0QNj2I5R5wNCt8d1I2Lc.BzCU5413sBDuxCM4zukf/lfrygEbwGC', 'user');


--
-- Data for Name: counters; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.counters VALUES
	(7, 5, 'x', '2023-07-01 00:00:00', 30, 4, 0),
	(8, 2, 'o', '2023-07-01 00:00:00', 4, 29, 0),
	(5, 2, 'x', '2023-07-01 00:00:00', 8, 2, 0),
	(3, 2, 'x', '2023-06-01 00:00:00', 1, 2, 0),
	(4, 5, 'o', '2023-06-01 00:00:00', 2, 1, 0),
	(6, 5, 'o', '2023-07-01 00:00:00', 2, 7, 0),
	(1, 5, 'x', '2023-06-01 00:00:00', 19, 0, 1),
	(2, 2, 'o', '2023-06-01 00:00:00', 0, 19, 1),
	(10, 6, 'o', '2023-07-01 00:00:00', 0, 1, 0),
	(9, 6, 'x', '2023-07-01 00:00:00', 2, 0, 0),
	(11, 69, 'o', '2023-07-01 00:00:00', 0, 3, 0);


--
-- Data for Name: games; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.games VALUES
	(53, 5, 2, false, '2023-07-05 09:19:04.775', '2023-07-05 09:19:17.778', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", "x", "x", null, null], [null, null, "x", "o", "o", "x", null], [null, null, "x", "o", "o", "o", "o"]], "gameOver": [[[5, 5], [5, 4], [5, 3], [5, 6]], ["o"]]}'),
	(54, 5, 2, true, '2023-07-05 09:24:32.784', '2023-07-05 09:28:05.588', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(55, 5, 2, true, '2023-07-05 09:44:36.003', '2023-07-05 09:46:30.911', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, null, "x", null, null, null], [null, null, null, "x", "o", null, null], [null, null, null, "x", "o", null, null], [null, null, null, "x", "o", null, null]], "gameOver": [[[3, 3], [4, 3], [5, 3], [2, 3]], ["x"]]}'),
	(56, 5, 2, true, '2023-07-05 09:55:32.61', '2023-07-05 09:55:51.347', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(57, 5, 2, true, '2023-07-05 09:59:04.358', '2023-07-05 09:59:12.835', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(58, 5, 2, true, '2023-07-05 10:00:11.383', '2023-07-05 10:01:52.837', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(59, 5, 2, true, '2023-07-05 10:01:58.468', '2023-07-05 10:02:31.436', '{"field": [[null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "o", "x", null, null, null]], "gameOver": [[[2, 2], [3, 2], [4, 2], [1, 2]], ["x"]]}'),
	(60, 5, 2, true, '2023-07-05 10:04:52.924', '2023-07-05 10:04:59.225', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(61, 5, 2, true, '2023-07-05 10:05:04.811', '2023-07-05 10:05:23.307', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(63, 5, 2, true, '2023-07-05 14:42:40.853', '2023-07-05 14:47:03.4', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, "x", null, null, null, null, null], [null, "x", "o", null, null, null, null], [null, "x", "o", null, null, null, null], [null, "x", "o", null, null, null, null]], "gameOver": [[[3, 1], [4, 1], [5, 1], [2, 1]], ["x"]]}'),
	(47, 5, 2, true, '2023-06-30 11:59:48.308', '2023-06-30 12:00:42.403', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], ["x", null, null, null, null, null, null], ["x", null, "o", null, null, null, null], ["x", null, "o", null, null, null, null], ["x", null, "o", null, null, null, null]], "gameOver": [[[3, 0], [4, 0], [5, 0], [2, 0]], ["x"]]}'),
	(64, 5, 2, true, '2023-07-06 11:39:31.885', '2023-07-06 11:39:40.751', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, "x", null, null, null, null, null], [null, "x", "o", null, null, null, null], [null, "x", "o", null, null, null, null], [null, "x", "o", null, null, null, null]], "gameOver": [[[3, 1], [4, 1], [5, 1], [2, 1]], ["x"]]}'),
	(48, 2, 5, false, '2023-07-01 09:49:00.995', '2023-07-01 09:50:42.226', '{"field": [[null, null, null, "o", null, null, null], [null, null, null, "x", null, null, null], [null, "o", "x", "o", null, null, null], [null, "x", "o", "x", null, null, null], ["x", "o", "x", "o", "x", null, null], ["o", "x", "o", "x", "o", null, null]], "gameOver": [[[3, 2], [4, 3], [5, 4], [2, 1]], ["o"]]}'),
	(49, 5, 2, true, '2023-07-02 10:10:19.341', '2023-07-02 10:10:42.921', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, "x", null, null, null, null, null], [null, "x", "o", null, null, null, null], [null, "x", "o", null, null, null, null], [null, "x", "o", null, null, null, null]], "gameOver": [[[3, 1], [4, 1], [5, 1], [2, 1]], ["x"]]}'),
	(50, 5, 2, false, '2023-07-02 10:19:48.19', '2023-07-02 10:19:57.239', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, null, "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], ["x", null, "x", "o", null, null, null]], "gameOver": [[[3, 3], [4, 3], [5, 3], [2, 3]], ["o"]]}'),
	(51, 5, 2, true, '2023-07-02 10:20:33.662', '2023-07-02 10:20:41.782', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], ["x", null, null, null, null, null, null], ["x", null, null, null, "o", null, null], ["x", null, null, null, "o", null, null], ["x", null, null, null, "o", null, null]], "gameOver": [[[3, 0], [4, 0], [5, 0], [2, 0]], ["x"]]}'),
	(65, 2, 5, true, '2023-07-06 11:41:56.941', '2023-07-06 11:42:09.97', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, null, "x", null, null, null], [null, null, "o", "x", null, null, null], [null, null, "o", "x", null, null, null], [null, null, "o", "x", null, null, null]], "gameOver": [[[3, 3], [4, 3], [5, 3], [2, 3]], ["x"]]}'),
	(66, 5, 2, true, '2023-07-06 17:14:55.139', '2023-07-06 17:18:05.53', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, null, "x", null, null, null], [null, null, "o", "x", null, null, null], [null, null, "o", "x", null, null, null], [null, null, "o", "x", null, null, null]], "gameOver": [[[3, 3], [4, 3], [5, 3], [2, 3]], ["x"]]}'),
	(67, 6, 2, true, '2023-07-07 12:06:55.926', '2023-07-07 12:08:38.141', '{"field": [[null, null, null, null, null, null, null], [null, null, null, "x", null, null, null], [null, null, "o", "x", null, null, null], [null, null, "o", "x", null, null, null], [null, null, "o", "x", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[2, 3], [3, 3], [4, 3], [1, 3]], ["x"]]}'),
	(68, 5, 2, true, '2023-07-07 14:09:22.665', '2023-07-07 14:09:57.674', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(69, 5, 2, true, '2023-07-07 14:23:31.008', '2023-07-07 14:24:06.249', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(70, 2, 5, true, '2023-07-07 14:26:51.544', '2023-07-07 14:27:59.824', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "o", null, null, null, null], [null, null, "o", null, null, null, null], [null, null, "o", null, null, null, null], [null, null, "x", "x", "x", "x", null]], "gameOver": [[[5, 4], [5, 3], [5, 2], [5, 5]], ["x"]]}'),
	(71, 5, 2, true, '2023-07-10 08:39:00.364', '2023-07-10 08:44:54.608', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(72, 5, 2, true, '2023-07-10 08:44:58.332', '2023-07-10 08:45:08.614', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(73, 5, 2, true, '2023-07-10 08:45:12.277', '2023-07-10 08:46:22.957', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "x", null, null, null], [null, null, "x", "o", "o", null, null], [null, null, "x", "o", "o", null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(74, 5, 2, false, '2023-07-10 08:46:26.326', '2023-07-10 08:50:23.859', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, null, "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", "x", "o", null], [null, "x", "x", "o", "x", "o", null]], "gameOver": [[[3, 3], [4, 3], [5, 3], [2, 3]], ["o"]]}'),
	(75, 5, 2, true, '2023-07-10 08:50:36.05', '2023-07-10 08:51:10.127', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, "x", "x", "o", "o", null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(76, 5, 2, true, '2023-07-10 08:52:20.087', '2023-07-10 08:52:29.968', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(77, 5, 2, false, '2023-07-10 08:52:32.834', '2023-07-10 08:52:58.858', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, null, "o", null, null, null], [null, "x", "x", "o", null, null, null], [null, "o", "x", "o", null, null, null], [null, "x", "x", "o", null, null, null]], "gameOver": [[[3, 3], [4, 3], [5, 3], [2, 3]], ["o"]]}'),
	(78, 2, 6, true, '2023-07-10 08:56:38.762', '2023-07-10 08:56:48', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(79, 6, 2, true, '2023-07-10 08:56:55.653', '2023-07-10 08:57:30.454', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(80, 5, 69, true, '2023-07-10 10:07:55.042', '2023-07-10 10:08:16.201', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, "x", "x", "o", "o", null, null], ["x", "x", "x", "o", "o", "o", null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(81, 5, 69, true, '2023-07-10 10:08:36.574', '2023-07-10 10:08:43.72', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", null, "o", null, null], [null, null, "x", null, "o", null, null], [null, null, "x", null, "o", null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(82, 5, 69, true, '2023-07-10 10:08:56.32', '2023-07-10 10:09:01.244', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(83, 5, 2, true, '2023-07-10 15:29:20.815', '2023-07-10 15:35:52.591', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(84, 5, 2, true, '2023-07-10 15:50:27.55', '2023-07-10 15:51:12.871', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(85, 5, 2, true, '2023-07-13 11:18:59.303', '2023-07-13 11:19:22.419', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "o", "o", "o", null, null], [null, null, "x", "x", "x", "x", null], [null, null, "x", "x", "o", "o", null], [null, "x", "o", "x", "o", "x", "o"]], "gameOver": [[[3, 4], [3, 3], [3, 2], [3, 5]], ["x"]]}'),
	(86, 5, 2, true, '2023-07-13 11:19:30.684', '2023-07-13 11:20:56.725', '{"field": [[null, null, null, "x", null, null, null], [null, null, null, "o", null, null, null], [null, null, "o", "o", null, "x", null], [null, null, "o", "o", "o", "x", null], [null, "x", "x", "x", "x", "o", null], [null, "o", "x", "x", "o", "x", null]], "gameOver": [[[4, 2], [4, 3], [4, 4], [4, 1]], ["x"]]}'),
	(89, 2, 5, false, '2023-07-17 11:33:52.058', '2023-07-17 11:35:02.001', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, "o", "o", "x", null, null, null], [null, "o", "x", "o", "x", null, null], [null, "o", "x", "o", "x", null, null], [null, "o", "x", "o", "x", "x", null]], "gameOver": [[[3, 1], [4, 1], [5, 1], [2, 1]], ["o"]]}'),
	(90, 2, 5, true, '2023-07-17 14:05:20.984', '2023-07-17 14:07:13.928', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(88, 5, 2, true, '2023-07-16 11:33:06.14', '2023-07-16 11:33:15.551', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, "x", "x", "o", null, null, "o"]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(91, 2, 5, true, '2023-07-17 14:09:48.756', '2023-07-17 14:10:49.548', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, "x", null, null, null, null, null], [null, "x", "o", null, null, null, null], [null, "x", "o", null, null, null, null], [null, "x", "o", null, null, null, null]], "gameOver": [[[3, 1], [4, 1], [5, 1], [2, 1]], ["x"]]}'),
	(87, 5, 2, true, '2023-07-15 18:59:02.945', '2023-07-15 18:59:08.674', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(92, 2, 5, true, '2023-07-17 14:26:54.343', '2023-07-17 14:27:51.485', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(93, 5, 2, true, '2023-07-17 14:27:59.482', '2023-07-17 14:28:06.17', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, null, "x", null, null, null], [null, null, null, "x", "o", null, null], [null, null, null, "x", "o", null, null], [null, null, null, "x", "o", null, null]], "gameOver": [[[3, 3], [4, 3], [5, 3], [2, 3]], ["x"]]}'),
	(94, 2, 5, true, '2023-07-18 09:02:04.572', '2023-07-18 09:07:25.812', '{"field": [[null, null, null, null, null, null, null], [null, null, null, "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "x", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}'),
	(95, 2, 5, true, '2023-07-18 09:29:13.447', '2023-07-18 09:42:28.173', '{"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], [null, null, "x", null, null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null], [null, null, "x", "o", null, null, null]], "gameOver": [[[3, 2], [4, 2], [5, 2], [2, 2]], ["x"]]}');


--
-- Data for Name: knex_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.knex_migrations VALUES
	(2, '20230625134751_create_table.js', 1, '2023-06-25 19:14:23.277+05'),
	(3, '20230629125313_create_table_counters.js', 2, '2023-06-29 17:54:46.153+05');


--
-- Data for Name: knex_migrations_lock; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.knex_migrations_lock VALUES
	(1, 0);


--
-- Data for Name: messenger; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.messenger VALUES
	(47, 5, 'asdasd', 'x', '2023-06-30 12:00:17.429'),
	(47, 5, 'asdasdas', 'x', '2023-06-30 12:00:18.589'),
	(47, 5, 'asdasdasd', 'x', '2023-06-30 12:00:19.804'),
	(47, 2, 'asdasd', 'o', '2023-06-30 12:00:21.172'),
	(47, 2, 'asdasd', 'o', '2023-06-30 12:00:22.076'),
	(47, 2, 'asdasd', 'o', '2023-06-30 12:00:23.094'),
	(47, 5, 'sdfsdf', 'x', '2023-06-30 12:00:33.017'),
	(47, 5, 'sdfsdfsd', 'x', '2023-06-30 12:00:33.961'),
	(48, 5, 'dsfsdfsd', 'o', '2023-07-01 09:49:23.611'),
	(48, 5, 'sdfsdfsdf', 'o', '2023-07-01 09:49:24.7'),
	(48, 5, 'sdfsdf', 'o', '2023-07-01 09:49:26.115'),
	(48, 2, 'sdfsdfsd', 'x', '2023-07-01 09:49:27.425'),
	(48, 2, 'sdfsdf', 'x', '2023-07-01 09:49:28.399'),
	(48, 2, 'sdfsdfsd', 'x', '2023-07-01 09:49:29.371'),
	(48, 2, 'sdfsdf', 'x', '2023-07-01 09:49:30.179'),
	(48, 5, 'sdfsdfsd', 'o', '2023-07-01 09:49:32.521'),
	(48, 2, 'sdfsdfsd', 'x', '2023-07-01 09:49:34.078'),
	(48, 2, 'sdfsdf', 'x', '2023-07-01 09:49:34.921'),
	(48, 5, 'sdfsdfsd', 'o', '2023-07-01 09:49:36.253'),
	(49, 5, 'sdfsdf', 'x', '2023-07-02 10:10:25.359'),
	(49, 5, '1', 'x', '2023-07-02 10:10:26.918'),
	(49, 5, '2', 'x', '2023-07-02 10:10:27.666'),
	(49, 5, '3', 'x', '2023-07-02 10:10:28.265'),
	(49, 2, '4', 'o', '2023-07-02 10:10:32.455'),
	(49, 2, '5', 'o', '2023-07-02 10:10:33.307'),
	(49, 2, '6', 'o', '2023-07-02 10:10:34.125'),
	(49, 2, '7', 'o', '2023-07-02 10:10:35.012'),
	(49, 2, '8', 'o', '2023-07-02 10:10:35.824'),
	(49, 5, '9', 'x', '2023-07-02 10:10:38.251'),
	(63, 5, 'sdfsdf', 'x', '2023-07-05 14:42:45.513'),
	(63, 5, 'sdfsdfsdfsdf', 'x', '2023-07-05 14:42:47.847'),
	(63, 5, 'sdfsdfsdfsdf', 'x', '2023-07-05 14:42:56.344'),
	(63, 5, 'werwerwe', 'x', '2023-07-05 14:42:57.572'),
	(63, 5, 'sdfsdfsd', 'x', '2023-07-05 14:42:58.701'),
	(63, 5, 'sdfsdfsd', 'x', '2023-07-05 14:43:00.168'),
	(63, 5, 'sdfsdfsdf', 'x', '2023-07-05 14:43:01.664'),
	(63, 5, 'werwerwer', 'x', '2023-07-05 14:43:03.032'),
	(63, 5, 'sdfsdfsd', 'x', '2023-07-05 14:43:04.474'),
	(63, 5, 'werwercxv', 'x', '2023-07-05 14:43:06.101'),
	(63, 5, 'sdfsdfsdf', 'x', '2023-07-05 14:43:09.791'),
	(63, 2, 'sdfsdfsdfsd', 'o', '2023-07-05 14:43:13.522'),
	(63, 2, 'werwexcvx', 'o', '2023-07-05 14:43:15.215'),
	(63, 2, 'sdfsdfs', 'o', '2023-07-05 14:43:16.199'),
	(63, 2, 'ewrwexdfsdfsdfxcv', 'o', '2023-07-05 14:43:18.854'),
	(68, 5, 'ываываыв', 'x', '2023-07-07 14:09:24.563'),
	(68, 5, 'ываыва', 'x', '2023-07-07 14:09:25.51'),
	(68, 5, 'ваыва', 'x', '2023-07-07 14:09:27.445'),
	(68, 5, 'ываыва', 'x', '2023-07-07 14:09:29.163'),
	(68, 5, 'ываываываываы', 'x', '2023-07-07 14:09:31.201'),
	(68, 5, 'цукцукцук', 'x', '2023-07-07 14:09:32.719'),
	(68, 5, 'выаываываы', 'x', '2023-07-07 14:09:34.174'),
	(68, 5, 'чсчсм', 'x', '2023-07-07 14:09:35.31'),
	(68, 5, 'цукцукцу', 'x', '2023-07-07 14:09:36.502'),
	(68, 5, 'ываываы', 'x', '2023-07-07 14:09:37.773'),
	(68, 5, 'цукцук', 'x', '2023-07-07 14:09:38.704'),
	(68, 5, 'ываываыва', 'x', '2023-07-07 14:09:39.821'),
	(69, 5, 'erterte', 'x', '2023-07-07 14:23:34.611'),
	(69, 5, 'erterterter', 'x', '2023-07-07 14:23:36.598'),
	(69, 5, 'ertert', 'x', '2023-07-07 14:23:37.642'),
	(69, 2, 'erterte', 'o', '2023-07-07 14:23:39.458'),
	(69, 2, 'erterter', 'o', '2023-07-07 14:23:40.909'),
	(69, 2, 'erterte', 'o', '2023-07-07 14:23:42.185'),
	(69, 2, 'erterte', 'o', '2023-07-07 14:23:43.454'),
	(69, 5, 'erterter', 'x', '2023-07-07 14:23:45.708'),
	(69, 5, 'erterter', 'x', '2023-07-07 14:23:46.859'),
	(69, 5, 'ertertertert', 'x', '2023-07-07 14:23:48.459'),
	(69, 5, 'erterterterterter', 'x', '2023-07-07 14:23:50.351'),
	(69, 2, 'erterterterterter', 'o', '2023-07-07 14:23:53.514'),
	(70, 5, 'fsdfsdfsdfsdfsdfsdfsdfdfsdfsdfsdfsdfsdfsdf', 'o', '2023-07-07 14:26:56.146'),
	(70, 5, 'sdfsdfsdfs', 'o', '2023-07-07 14:26:59.897'),
	(70, 5, 'sdfsfsdf', 'o', '2023-07-07 14:27:01.013'),
	(70, 5, 'sdfsdfsdf sdfsd dfdf dfdfd fdfdfd dfdfdf dfdfdf dfdsdfs sdfsfds werwerwer  werwerwer wer13212afs werwersdfs', 'o', '2023-07-07 14:27:13.958'),
	(70, 2, 'dfgdfgdfg', 'x', '2023-07-07 14:27:24.465'),
	(70, 2, 'dfgdfgdfgdf', 'x', '2023-07-07 14:27:26.152'),
	(70, 2, 'dfgdfgdfgd', 'x', '2023-07-07 14:27:27.528'),
	(70, 2, 'ertertet', 'x', '2023-07-07 14:27:28.677'),
	(70, 2, 'vcbcvb', 'x', '2023-07-07 14:27:29.832'),
	(70, 2, 'fdgfdgdfgdfgdf', 'x', '2023-07-07 14:27:31.399'),
	(79, 6, 'sdfsd', 'x', '2023-07-10 08:57:09.407'),
	(79, 6, 'sdfsdfs', 'x', '2023-07-10 08:57:10.391'),
	(79, 6, 'sdfsdf', 'x', '2023-07-10 08:57:11.223'),
	(79, 2, 'sdfsdf', 'o', '2023-07-10 08:57:12.592'),
	(79, 2, 'sfsdf', 'o', '2023-07-10 08:57:13.875'),
	(79, 2, 'sdfsd', 'o', '2023-07-10 08:57:14.799'),
	(79, 2, 'sdfsd', 'o', '2023-07-10 08:57:15.67'),
	(79, 2, 'sfsdfs', 'o', '2023-07-10 08:57:16.575'),
	(79, 6, 'sdfsdf', 'x', '2023-07-10 08:57:17.963'),
	(79, 6, 'sdfsdf', 'x', '2023-07-10 08:57:18.81'),
	(79, 6, 'sdfsdfsd', 'x', '2023-07-10 08:57:19.823'),
	(79, 6, 'sdfs', 'x', '2023-07-10 08:57:22.138'),
	(79, 6, '3242342', 'x', '2023-07-10 08:57:23.138'),
	(83, 5, 'sdfsdfsd', 'x', '2023-07-10 15:29:22.627'),
	(83, 5, 'sdfsdf', 'x', '2023-07-10 15:29:23.663'),
	(83, 5, 'sdfsd', 'x', '2023-07-10 15:29:25.245'),
	(83, 2, 'sdfsdfsd', 'o', '2023-07-10 15:29:27.235'),
	(83, 2, 'sdfsdf', 'o', '2023-07-10 15:29:28.142'),
	(83, 2, 'sdfsdfs', 'o', '2023-07-10 15:29:29.093'),
	(83, 2, 'sdfsdf', 'o', '2023-07-10 15:29:30.074'),
	(83, 5, 'sdfsd', 'x', '2023-07-10 15:29:31.993'),
	(83, 5, 'sdfsdf', 'x', '2023-07-10 15:29:33.015'),
	(83, 5, 'sdfsdsd', 'x', '2023-07-10 15:29:33.955'),
	(84, 5, 'fdgdfgdf', 'x', '2023-07-10 15:50:36.596'),
	(84, 5, 'dfgdfgdf', 'x', '2023-07-10 15:50:38.078'),
	(84, 5, 'dfgdfgdfg', 'x', '2023-07-10 15:50:39.333'),
	(84, 5, 'sdfsdfsd', 'x', '2023-07-10 15:50:43.604'),
	(84, 5, 'cvbcvbcv', 'x', '2023-07-10 15:50:44.84'),
	(84, 2, 'sdfsdfs', 'o', '2023-07-10 15:50:48.865'),
	(84, 2, 'sdfsdfxcvxc', 'o', '2023-07-10 15:50:50.554'),
	(84, 2, 'werwerwe', 'o', '2023-07-10 15:50:51.782'),
	(84, 2, 'sdfsdfsdfsdfsd', 'o', '2023-07-10 15:50:53.408'),
	(84, 2, 'cvbcvbcv', 'o', '2023-07-10 15:50:55.07'),
	(84, 5, 'sdfsdfsdf', 'x', '2023-07-10 15:51:00.577'),
	(84, 5, 'xcvxcvxc', 'x', '2023-07-10 15:51:01.695'),
	(84, 5, 'dsfsdfxcvxc', 'x', '2023-07-10 15:51:03.342'),
	(86, 5, 'Привет', 'x', '2023-07-13 11:19:39.221'),
	(86, 2, 'Ну примет', 'o', '2023-07-13 11:19:48.052'),
	(86, 2, 'привет*', 'o', '2023-07-13 11:19:59.138'),
	(86, 5, 'Готов проиграть?', 'x', '2023-07-13 11:20:06.684'),
	(86, 2, 'А ты?', 'o', '2023-07-13 11:20:10.598'),
	(90, 5, 'fdgdfg', 'o', '2023-07-17 14:05:30.085'),
	(90, 5, 'dfgdfgfd', 'o', '2023-07-17 14:05:31.179'),
	(90, 5, 'gdfgdfgd', 'o', '2023-07-17 14:05:35.425'),
	(90, 2, 'dfgdfg', 'x', '2023-07-17 14:05:37.059'),
	(90, 2, 'dfgdfgdf', 'x', '2023-07-17 14:05:38.445'),
	(90, 2, 'dfgdfgdf', 'x', '2023-07-17 14:05:39.799'),
	(90, 2, 'dfgdfgd', 'x', '2023-07-17 14:05:40.954'),
	(90, 5, 'dfgdfgdf', 'o', '2023-07-17 14:05:43.775'),
	(90, 5, 'dfgdfgdfgdf', 'o', '2023-07-17 14:05:46.247'),
	(90, 5, 'dfgdfgdfgdfcvb', 'o', '2023-07-17 14:05:49.264'),
	(90, 2, 'dfgdfretertercvb', 'x', '2023-07-17 14:05:54.936'),
	(90, 2, 'dfgdfgcvb', 'x', '2023-07-17 14:05:57.291'),
	(90, 5, 'sdfsdfsd', 'o', '2023-07-17 14:06:21.629'),
	(90, 5, 'sdfsdfsd', 'o', '2023-07-17 14:06:23.069'),
	(90, 2, 'xcvxcsdfwerwe', 'x', '2023-07-17 14:06:26.754'),
	(90, 2, 'sdfsdfsdfxcv', 'x', '2023-07-17 14:06:28.447'),
	(90, 5, 'erwerwe', 'o', '2023-07-17 14:06:30.922'),
	(90, 5, 'sdfsdfxcv', 'o', '2023-07-17 14:06:32.524'),
	(90, 5, 'werwerwerwe', 'o', '2023-07-17 14:06:34.179'),
	(91, 5, 'ghfg', 'o', '2023-07-17 14:09:51.971'),
	(91, 5, 'dfgdfgdf', 'o', '2023-07-17 14:09:53.269'),
	(91, 5, 'cbcvbcvb', 'o', '2023-07-17 14:09:54.498'),
	(91, 5, 'reterte', 'o', '2023-07-17 14:09:55.614'),
	(91, 5, 'fgdfgdf', 'o', '2023-07-17 14:09:56.718'),
	(91, 5, 'cvbcvb', 'o', '2023-07-17 14:09:57.806'),
	(91, 5, 'dfgdfgdfgdfgdfcvbcv', 'o', '2023-07-17 14:10:00.735'),
	(91, 2, 'dfgdfg', 'x', '2023-07-17 14:10:03.733'),
	(91, 2, 'erterter', 'x', '2023-07-17 14:10:04.832'),
	(91, 5, 'dfgdfgdf', 'o', '2023-07-17 14:10:09.928'),
	(91, 5, 'cvbcvberte', 'o', '2023-07-17 14:10:11.458'),
	(91, 2, 'dfgdfgdf', 'x', '2023-07-17 14:10:13.282'),
	(91, 5, 'dfgdfgertert', 'o', '2023-07-17 14:10:19.553'),
	(91, 5, 'dfgdfgcvb', 'o', '2023-07-17 14:10:21.176'),
	(91, 5, 'rtertertdfgd', 'o', '2023-07-17 14:10:22.95'),
	(91, 2, 'dfgdfgd', 'x', '2023-07-17 14:10:24.742'),
	(91, 2, 'ertertvbcv', 'x', '2023-07-17 14:10:26.349'),
	(91, 2, 'dfgdfgerter', 'x', '2023-07-17 14:10:27.816'),
	(91, 5, 'fdgdfgdcvb', 'o', '2023-07-17 14:10:30.201'),
	(91, 5, 'dfgdfg', 'o', '2023-07-17 14:10:37.871'),
	(91, 5, 'vbcvbcvb', 'o', '2023-07-17 14:10:39.269'),
	(91, 5, 'ertert', 'o', '2023-07-17 14:10:40.756');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES
	(5, 'Цефар', 'Комаров', 'Александрович', 145, true, true, '2023-06-25 15:21:14.842', '2023-06-25 15:21:14.842'),
	(4, 'Цефар', 'Комаров', 'Александрович', 145, true, true, '2023-06-25 14:57:05.32', '2023-07-02 15:15:47.697'),
	(14, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:27:54.173', '2023-06-27 11:27:54.173'),
	(16, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:27:58.119', '2023-06-27 11:27:58.119'),
	(22, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:28:08.42', '2023-06-27 11:28:08.42'),
	(30, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:32.812', '2023-06-27 11:30:32.812'),
	(34, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:36.951', '2023-06-27 11:30:36.951'),
	(40, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:44.376', '2023-06-27 11:30:44.376'),
	(42, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:49.021', '2023-06-27 11:30:49.021'),
	(44, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:58.884', '2023-06-27 11:30:58.884'),
	(46, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:31:00.568', '2023-06-27 11:31:00.568'),
	(48, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:31:04.434', '2023-06-27 11:31:04.434'),
	(50, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:31:07.026', '2023-06-27 11:31:07.026'),
	(52, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:31:08.604', '2023-06-27 11:31:08.604'),
	(60, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:31:30.679', '2023-06-27 11:31:30.679'),
	(62, 'Станислав', 'Юрченко', 'Грегорьевич', 25, true, false, '2023-06-27 11:31:34.479', '2023-07-02 15:50:51.446'),
	(59, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:29.993', '2023-06-27 11:31:29.993'),
	(57, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:27.18', '2023-06-27 11:31:27.18'),
	(55, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:25.568', '2023-07-06 10:14:58.178'),
	(47, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:02.68', '2023-06-27 11:31:02.68'),
	(49, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:06.226', '2023-07-06 10:14:48.918'),
	(51, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:07.792', '2023-06-27 11:31:07.792'),
	(37, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:39.599', '2023-06-27 11:30:39.599'),
	(39, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:43.66', '2023-07-02 11:55:17.784'),
	(33, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:35.462', '2023-06-27 11:30:35.462'),
	(27, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:27.238', '2023-06-27 11:30:27.238'),
	(19, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:28:03.905', '2023-06-27 11:28:03.905'),
	(15, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:27:56.322', '2023-06-27 11:27:56.322'),
	(21, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:28:06.64', '2023-07-02 15:22:01.897'),
	(11, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:27:35.771', '2023-07-02 15:21:47.132'),
	(17, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:28:00.468', '2023-07-02 15:21:51.053'),
	(1, 'Цефар', 'Комаров', 'Александрович', 145, true, false, '2023-06-25 14:53:33.105', '2023-06-25 14:53:33.105'),
	(3, 'бла', 'бла', 'бла', 34, true, true, '2023-06-25 14:55:39.639', '2023-07-02 15:49:09.944'),
	(13, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:27:41.736', '2023-06-27 11:27:41.736'),
	(56, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:26.412', '2023-07-02 11:55:19.951'),
	(18, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:28:02.564', '2023-07-02 15:21:52.987'),
	(20, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:28:04.925', '2023-06-27 11:28:04.925'),
	(9, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:27:28.417', '2023-07-02 15:22:08.111'),
	(25, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:24.375', '2023-06-27 11:30:24.375'),
	(26, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:25.594', '2023-06-27 11:30:25.594'),
	(29, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:31.901', '2023-06-27 11:30:31.901'),
	(31, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:33.798', '2023-06-27 11:30:33.798'),
	(32, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:34.63', '2023-06-27 11:30:34.63'),
	(35, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:38.052', '2023-06-27 11:30:38.052'),
	(36, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:38.847', '2023-06-27 11:30:38.847'),
	(38, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:42.668', '2023-06-27 11:30:42.668'),
	(41, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:48.189', '2023-06-27 11:30:48.189'),
	(43, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:57.933', '2023-06-27 11:30:57.933'),
	(45, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:30:59.707', '2023-06-27 11:30:59.707'),
	(53, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:09.367', '2023-06-27 11:31:09.367'),
	(63, 'Александра', 'Измайлова', 'Вячеславовна', 21, false, true, '2023-06-27 11:31:35.262', '2023-07-02 11:55:27.317'),
	(64, 'Дарья', 'Беднягина', 'Ивановна', 18, false, false, '2023-06-27 11:31:35.947', '2023-07-18 09:09:14.903'),
	(66, 'Цефар', 'Долголетов', 'Антонович', 12, true, true, '2023-06-27 11:31:37.45', '2023-07-18 09:09:13.924'),
	(68, 'Иван', 'Вязкин', 'Анатольевич', 23, true, true, '2023-07-10 09:31:34.635', '2023-07-10 09:31:34.635'),
	(2, 'Максим', 'Алексеев', 'Юрьевич', 40, true, true, '2023-06-25 14:54:11.074', '2023-07-02 15:56:42.943'),
	(6, 'Иван', 'Иванов', 'Иванович', 23, false, true, '2023-06-25 15:48:58.976', '2023-07-02 15:14:50.146'),
	(8, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:27:18.906', '2023-07-02 15:14:48.24'),
	(28, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:28.725', '2023-07-02 11:55:16.415'),
	(54, 'Цефар', 'Комаров', 'Александрович', 145, false, false, '2023-06-27 11:31:11.488', '2023-07-06 10:14:51.071'),
	(10, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:27:32.477', '2023-07-02 15:21:47.912'),
	(23, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:28:10.541', '2023-07-02 15:21:58.826'),
	(12, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:27:39.073', '2023-07-02 15:21:46.351'),
	(24, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:30:22.932', '2023-07-02 15:21:56.758'),
	(61, 'Константин', 'Черный', 'Алексеевич', 31, true, false, '2023-06-27 11:31:33.315', '2023-06-27 11:31:33.315'),
	(58, 'Цефар', 'Комаров', 'Александрович', 145, false, true, '2023-06-27 11:31:27.915', '2023-07-07 12:46:37.113'),
	(67, 'Александр', 'Бурков', 'Юрьевич', 34, true, false, '2023-07-01 09:48:22.773', '2023-07-09 13:31:40.118'),
	(69, 'Вячеслав', 'Губкин', 'Сергеевич', 20, true, true, '2023-07-10 10:07:32.079', '2023-07-18 09:09:23.932'),
	(65, 'Цефар', 'Комаров', 'Александрович', 42, true, true, '2023-06-27 11:31:36.682', '2023-07-02 11:55:27.752');


--
-- Name: counters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.counters_id_seq', 11, true);


--
-- Name: games_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.games_id_seq', 95, true);


--
-- Name: knex_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.knex_migrations_id_seq', 3, true);


--
-- Name: knex_migrations_lock_index_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.knex_migrations_lock_index_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 69, true);


--
-- Name: accounts accounts_iduser_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_iduser_unique UNIQUE ("idUser");


--
-- Name: accounts accounts_login_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_login_unique UNIQUE (login);


--
-- Name: counters counters_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.counters
    ADD CONSTRAINT counters_pkey PRIMARY KEY (id);


--
-- Name: counters counters_user_id_symbol_month_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.counters
    ADD CONSTRAINT counters_user_id_symbol_month_unique UNIQUE (user_id, symbol, month);


--
-- Name: games games_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_pkey PRIMARY KEY (id);


--
-- Name: knex_migrations_lock knex_migrations_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations_lock
    ADD CONSTRAINT knex_migrations_lock_pkey PRIMARY KEY (index);


--
-- Name: knex_migrations knex_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.knex_migrations
    ADD CONSTRAINT knex_migrations_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: accounts accounts_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.users(id);


--
-- Name: counters counters_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.counters
    ADD CONSTRAINT counters_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: games games_idplayer1_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_idplayer1_foreign FOREIGN KEY ("idPlayer1") REFERENCES public.users(id);


--
-- Name: games games_idplayer2_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_idplayer2_foreign FOREIGN KEY ("idPlayer2") REFERENCES public.users(id);


--
-- Name: messenger messenger_idgame_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger
    ADD CONSTRAINT messenger_idgame_foreign FOREIGN KEY ("idGame") REFERENCES public.games(id);


--
-- Name: messenger messenger_iduser_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger
    ADD CONSTRAINT messenger_iduser_foreign FOREIGN KEY ("idUser") REFERENCES public.users(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

