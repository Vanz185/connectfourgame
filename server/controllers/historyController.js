const knexConfig = require('../DB/knexfile');
const ApiError = require('../error/ApiError');
const knex = require('knex')(knexConfig.development)


class historyController {

    async getHistory(req, res, next){
        const { limit, page, id, startDate, endDate } = req.query
        let offset = page * limit - limit;
        

        const historyPlayers = await knex('games')
        .join('users AS one', 'one.id','=','games.idPlayer1')
        .join('users AS two', 'two.id','=','games.idPlayer2')
        .join('accounts AS acone', 'acone.idUser','=','games.idPlayer1')
        .join('accounts AS actwo', 'actwo.idUser','=','games.idPlayer2')
        .select('games.id AS id', 'one.firstName AS firstName1', 'one.surName AS surName1', 'one.secondName AS secondName1', 
        'two.firstName AS firstName2', 'two.surName AS surName2', 'two.secondName AS secondName2', 'games.gameResult AS gameResult', 
        'games.gameBegin AS gameBegin', 'games.gameEnd AS gameEnd', 'acone.login AS login1', 'actwo.login AS login2','games.winField')
        .where(function() {
            this.where('games.idPlayer1','=', id).orWhere('games.idPlayer2','=', id)
        })
        .andWhereRaw(`${startDate === undefined || startDate ==='' ? '': `games."gameEnd" >= '${new Date(new Date(startDate + ' 00:00:00').getTime() - 18000000).toISOString()}'`}`)
        .andWhereRaw(`${endDate === undefined || endDate ==='' ? '': `games."gameEnd" <= '${new Date(new Date(endDate + ' 23:59:59').getTime() - 18000000).toISOString()}'`}`)
        .offset(offset)
        .limit(limit)
        .orderBy('id', 'DESC')
        .catch(e => console.log(e))

        
        if (historyPlayers.length === 0) {
            return next(ApiError.NotFound('Нет пользователей'));
        } 
        const totalCount = await knex('games')
        .count('id')
        .where(function() {
            this.where('games.idPlayer1','=', id).orWhere('games.idPlayer2','=', id)
        })
        .andWhereRaw(`${startDate === undefined || startDate ==='' ? '': `games."gameEnd" >= '${new Date(new Date(startDate + ' 00:00:00').getTime() - 18000000).toISOString()}'`}`)
        .andWhereRaw(`${endDate === undefined || endDate ==='' ? '': `games."gameEnd" <= '${new Date(new Date(endDate + ' 23:59:59').getTime() - 18000000).toISOString()}'`}`)
        
        
        return res.status(200).json({historyPlayers:historyPlayers, totalCount: totalCount});
        
        
        
        
    }
}

module.exports = new historyController();
