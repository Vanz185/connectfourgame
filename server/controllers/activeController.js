const knexConfig = require('../DB/knexfile');
const ApiError = require('../error/ApiError');
const knex = require('knex')(knexConfig.development)


class activeController {

    async getActivePlayers(req, res){
        const activePlayers = await knex('users').where('statusActive', true).select()
        
        if (activePlayers.length === 0) {
            return next(ApiError.NotFound('Нет пользователей'));
        }   

        return res.json(activePlayers);
    }
}

module.exports = new activeController();


