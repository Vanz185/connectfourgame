const knexConfig = require('../DB/knexfile')
const knex = require('knex')(knexConfig.development)
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const ApiError = require('../error/ApiError');




class ListOfPlayersController {
    async getPlayers(req, res, next){
        const { limit, page, id, filter } = req.query
        let offset = page * limit - limit;

        

        const listOfPLayers = await knex('users').select('users.id AS id','users.firstName AS firstName','users.surName AS surName', 
        'users.secondName AS secondName', 'users.age AS age', 'users.gender AS gender', 'users.statusActive AS statusActive', 
        'users.createdAt AS createdAt', 'users.updatedAt AS updatedAt', 'accounts.login AS login')
        .join('accounts', 'users.id', '=', 'accounts.idUser')
        .whereNot('users.id','=', id)
        .andWhere(knex.raw(`"firstName" like '${filter.split(" ")[1] === undefined || filter.split(" ")[1] ===''? '': filter.split(" ")[1]}%'`))
        .andWhere(knex.raw(`"surName" like '${filter.split(" ")[0] === undefined || filter.split(" ")[0] ==='' ? '': filter.split(" ")[0]}%'`))
        .andWhere(knex.raw(`"secondName" like '${filter.split(" ")[2] === undefined || filter.split(" ")[2] ==='' ? '': filter.split(" ")[2]}%'`))
        .offset(offset)
        .limit(limit)
        .orderBy('users.createdAt', 'DESC')
        .catch(e=> console.log(e))
        if (listOfPLayers.length === 0) {
            return next(ApiError.NotFound('Нет пользователей'));
        } 

        const totalCount = await knex('users')
        .count('id')
        .whereNot('users.id','=', id)
        .andWhere(knex.raw(`"firstName" like '${filter.split(" ")[1] === undefined || filter.split(" ")[1] ==='' ? '': filter.split(" ")[1]}%'`))
        .andWhere(knex.raw(`"surName" like '${filter.split(" ")[0] === undefined || filter.split(" ")[0] ==='' ? '': filter.split(" ")[0]}%'`))
        .andWhere(knex.raw(`"secondName" like '${filter.split(" ")[2] === undefined || filter.split(" ")[2] ==='' ? '': filter.split(" ")[2]}%'`))
        
        

        return res.status(200).json({listOfPLayers:listOfPLayers, totalCount: totalCount});
        
    }

    async editActive(req, res, next){
        const { id, reverseStatus } = req.body
        const editActive = await knex('users').where('id', id)
        .update({statusActive: reverseStatus, updatedAt: new Date().toISOString()})
        .returning('*')

        if (editActive.length === 0){
            return res.status(500).json({  message: 'Неопределенная ошибка'  })
        } else {
            return res.status(200).json(editActive[0])
        }
        
    }

    async updatePlayer(req, res, next){
        const { id, editData } = req.body
        if (editData.fio.trim().split(/\s+/).length === 3 && editData.age !=='' && editData.gender !==''){
            const editPlayer = await knex('users').where('id', id)
            .update({surName: editData.fio.split(' ')[0], 
                firstName: editData.fio.split(' ')[1], 
                secondName: editData.fio.split(' ')[2],
                age: editData.age,
                gender:editData.gender,
                updatedAt: new Date().toISOString()})
            .returning('*')

            if (editPlayer.length === 0){
                return res.status(500).json({  message: 'Неопределенная ошибка'  })
            } else {
                return res.status(200).json(editPlayer[0])
            }
        } else {
            return next(ApiError.badRequest('Недостаточно данных или данные не пришли'))
        }
        
        
    }


}

module.exports = new ListOfPlayersController()