const knexConfig = require('../DB/knexfile')
const knex = require('knex')(knexConfig.development)
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const ApiError = require('../error/ApiError');

const generateJwt = (id, login, role) => { 
    return jwt.sign(
        { id, login, role }, 
        process.env.SECRET_KEY, 
        { expiresIn: '1h' });
}


class userController {
    async login(req, res, next){
        const {login, password} = req.body
        const user = await knex('accounts').where('login', login).select();
        if (user.length === 0){
            return next(ApiError.NotFound('Пользователь не найден'))
        }

        const userBlock = await knex('users').where('id', user[0].idUser).where('statusActive', false).select()
        if (userBlock.length !== 0) {
            return next(ApiError.badRequest('Пользователь заблокирован'));
        }

        let comparePassword = bcrypt.compareSync(password, user[0].password)
        if (!comparePassword) {
            return next(ApiError.badRequest('Указан неверный пароль'))
        }

        const token = generateJwt(user[0].idUser, user[0].login, user[0].role); 
        return res.json({ token });
    }

    async check(req, res, next){
        const userBlock = await knex('users').where('id', req.user.id).where('statusActive', false).select()
        if (userBlock.length !== 0) {
            return next(ApiError.badRequest('Пользователь заблокирован'));
        }
        
        const token = generateJwt(req.user.id, req.user.login, req.user.role); 
        return res.json({ token });
    }

    async addPlayer(req, res, next){
        const {surName, firstName, secondName, age, gender, login, password} = req.body
        if (!surName || !age || !firstName || !secondName || gender ==='' || !login || !password) { 
            return next(ApiError.badRequest('Некорректный ввод'));
        }

        const candidate = await knex('accounts').where('login', login).select(); 
        if (candidate.length !== 0) {
            return next(ApiError.badRequest('Пользователь существует'))
        }

        const hashPassword = await bcrypt.hash(password, 5);
        const date = new Date().toISOString()
        const users = await knex('users').insert([{surName: surName, firstName: firstName, 
            secondName: secondName, age: age, gender: gender, statusActive: true,
            createdAt: date, updatedAt: date}]).returning("*")
        const accounts = await knex('accounts').insert([
            {idUser: users[0].id, login: login, password: hashPassword , role: 'user'}
        ]).returning("*")
        const token = generateJwt(users[0].id, accounts[0].login, accounts[0].role); 
        return res.json({ token })
    }


}

module.exports = new userController()