const knexConfig = require('../DB/knexfile');
const ApiError = require('../error/ApiError');
const knex = require('knex')(knexConfig.development)

class RatingController {

    async getRatingPlayers(req, res, next) {
        const { limit, page, filter } = req.query
        let offset = page * limit - limit;

        const ratingPlayers = 
        await knex('counters')
        .join('users', 'users.id','=','counters.user_id')
        .select('user_id', 'users.firstName', 'users.surName', 'users.secondName',
        knex.raw(`SUM(wins_cnt + loses_cnt + draws_cnt) AS total_cnt`),
        knex.raw(`SUM(wins_cnt) AS wins_cnt`),
        knex.raw(`SUM(loses_cnt) AS loses_cnt`),
        knex.raw(`SUM(wins_cnt::float)/ SUM(wins_cnt + loses_cnt + draws_cnt) AS wins_prc`),
        )
        .where(knex.raw(`month >= date_trunc('month', now()) - interval '6 month'`))
        .andWhere('users.statusActive', true)
        .andWhere(knex.raw(`"firstName" like '${filter.split(" ")[1] === undefined || filter.split(" ")[1] ===''? '': filter.split(" ")[1]}%'`))
        .andWhere(knex.raw(`"surName" like '${filter.split(" ")[0] === undefined || filter.split(" ")[0] ==='' ? '': filter.split(" ")[0]}%'`))
        .andWhere(knex.raw(`"secondName" like '${filter.split(" ")[2] === undefined || filter.split(" ")[2] ==='' ? '': filter.split(" ")[2]}%'`))
        .offset(offset)
        .limit(limit)
        .groupByRaw(`user_id, users."firstName", users."surName", users."secondName"`)
        .havingRaw(`SUM(wins_cnt + loses_cnt + draws_cnt) > 50`)
        .orderByRaw(`SUM(wins_cnt * (case when symbol = 'x' then 0.9 else 1 end) - loses_cnt * (case when symbol = 'x' then 1.1 else 1 end) + draws_cnt * 0.25) / 
            count(distinct month) DESC`)
        
        .catch(e => console.log(e))
        
        if (ratingPlayers.length === 0) {
            return next(ApiError.NotFound('Нет пользователей'));
        }

        
        const totalCount  = await knex((knex('counters').select('user_id')
        .join('users', 'counters.user_id','=','users.id')
        .where(knex.raw(`month >= date_trunc('month', now()) - interval '6 month'`))
        .andWhere('users.statusActive', true)
        .andWhere(knex.raw(`"firstName" like '${filter.split(" ")[1] === undefined || filter.split(" ")[1] ===''? '': filter.split(" ")[1]}%'`))
        .andWhere(knex.raw(`"surName" like '${filter.split(" ")[0] === undefined || filter.split(" ")[0] ==='' ? '': filter.split(" ")[0]}%'`))
        .andWhere(knex.raw(`"secondName" like '${filter.split(" ")[2] === undefined || filter.split(" ")[2] ==='' ? '': filter.split(" ")[2]}%'`))
        .groupByRaw(`user_id, users."firstName", users."surName", users."secondName"`)
        .havingRaw(`SUM(wins_cnt + loses_cnt + draws_cnt) > 50`)).as('t1')).count('t1.user_id')
        

        return res.json({ratingPlayers: ratingPlayers, totalCount:totalCount});
            
    }
}

module.exports = new RatingController();