const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    
    if (req.method === 'OPTIONS') { 
        next();
    }

    try {
        
        const token = req.headers.authorization.split(' ')[1]; 

        if (!token) {
            res.status(401).json({ message: 'не авторизован' })
        }

        
        const decoded = jwt.verify(token, process.env.SECRET_KEY); 

        if(decoded.role !== "admin") {
            res.status(403).json({ message: 'нет доступа' })
        }

        req.user = decoded;
        next();
    }
    catch(error) {
        res.status(401).json({ message: 'не авторизован' })
    }
}