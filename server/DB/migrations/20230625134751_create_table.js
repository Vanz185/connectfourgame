/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema
    .createTable('users', table => {
        table.increments('id').primary().unique();
        table.string('firstName', 255).notNullable();
        table.string('surName', 255).notNullable();
        table.string('secondName', 255).notNullable();
        table.integer('age').notNullable().unsigned();
        table.boolean('gender').notNullable();
        table.boolean('statusActive').notNullable();
        table.boolean('statusGame').notNullable();
        table.timestamp('createdAt', { useTz: false }).notNullable();
        table.timestamp('updatedAt', { useTz: false }).notNullable();
    })
    .createTable('accounts', table => {
        table.integer('idUser').notNullable().unique().references('id').inTable('users');
        table.string('login', 255).notNullable().unique();
        table.string('password', 255).notNullable();
        table.string('role', 255);
    })
    .createTable('games', table => {
        table.increments('id').primary().unique();
        table.integer('idPlayer1').notNullable().references('id').inTable('users');
        table.integer('idPlayer2').notNullable().references('id').inTable('users');
        table.boolean('gameResult');
        table.timestamp('gameBegin', { useTz: false }).notNullable();
        table.timestamp('gameEnd', { useTz: false });
        table.jsonb('winField', 255)
    })
    .createTable('messenger', table => {
        table.integer('idGame').notNullable().references('id').inTable('games');
        table.integer('idUser').notNullable().references('id').inTable('users');
        table.string('message').notNullable();
        table.string('role').notNullable();
        table.timestamp('createdMesAt', { useTz: false }).notNullable();
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema
    .dropTable('accounts')
    .dropTable('messenger')
    .dropTable('games')
    .dropTable('users')
};
