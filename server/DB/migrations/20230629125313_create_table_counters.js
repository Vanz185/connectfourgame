/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function(knex) {
    await knex.schema
    .createTable('counters', table => {
        table.increments('id').primary();
        table.integer('user_id', 255).notNullable().references('id').inTable('users');
        table.specificType('symbol', 'CHAR(1)').notNullable();
        table.timestamp('month', { useTz: false }).notNullable().defaultTo(knex.raw(`date_trunc('month', now())`));
        table.integer('wins_cnt').notNullable().defaultTo(0)
        table.integer('loses_cnt').notNullable().defaultTo(0)
        table.integer('draws_cnt').notNullable().defaultTo(0)
        table.unique(['user_id','symbol','month'])
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down =async function(knex) {
    await knex.schema
    .dropTable('counters')
};
