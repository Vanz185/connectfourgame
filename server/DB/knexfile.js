// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {
  development: {
    client: 'pg',
    connection: {
      database: 'db_connect-four',
      user:     'postgres',
      password: '84625',
      host:'localhost',
      port:5432
    },
  },
  migrations: {
    directory: './migrations',
    tableName: 'knex_migrations'
  },

  seed: {
    directory: './seeds'
  }
};
