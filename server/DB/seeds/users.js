/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */

const { faker } = require('@faker-js/faker');


exports.seed = async function(knex) {
  
  const users = []

  for (let i = 0; i < 2000; i++) {
    users.push({
      surName: faker.person.lastName(),
      firstName: faker.person.firstName(),
      secondName: faker.person.middleName(),
      age: faker.number.int({ min: 5, max: 60 }),
      gender: faker.datatype.boolean(),
      statusActive: faker.datatype.boolean(),
      statusGame: faker.datatype.boolean(),
      createdAt: faker.date.between({from: '2022-01-01T00:00:00.000Z', to: '2022-08-01T00:00:00.000Z'}).toLocaleString(),
      updatedAt: faker.date.between({from: '2022-08-01T00:00:00.000Z', to: '2023-01-01T00:00:00.000Z'}).toLocaleString()
    });
  };
  


  await knex('users').insert(users);
  
  

  await knex('accounts').insert([
    {idUser: 1, login: 'log1', password: 'pas1', role: 'admin'},
    {idUser: 2, login: 'log2', password: 'pas2', role: 'user'},
    {idUser: 3, login: 'log3', password: 'pas3', role: 'user'},
    {idUser: 4, login: 'log4', password: 'pas4', role: 'user'},
    {idUser: 5, login: 'log5', password: 'pas5', role: 'user'},
    {idUser: 6, login: 'log6', password: 'pas6', role: 'user'},
    {idUser: 7, login: 'log7', password: 'pas7', role: 'user'},
    {idUser: 8, login: 'log8', password: 'pas8', role: 'user'},
    {idUser: 9, login: 'log9', password: 'pas9', role: 'user'},
    {idUser: 10, login: 'log10', password: 'pas10', role: 'user'},
  ]);

  const addGames = (count, start, end, minId, maxId ) => {
    let games = [];
    for (let i = 0; i < count; i++) {
        let endGame = faker.date.between({from: start, to: end})
        let fromStart = new Date(endGame.getTime() - (3600 * 1000))
        let toEnd = new Date(endGame.getTime() - (10 * 1000))
        let startGame = faker.date.between({from: fromStart, to: toEnd})
        if (minId === maxId){
            maxId = maxId + 1
        }
      games.push({  
        idPlayer1: faker.number.int({ min: minId, max: maxId }),
        idPlayer2: faker.number.int({ min: minId, max: maxId }),
        gameResult: faker.helpers.arrayElement([true, false, null]), 
        gameBegin: startGame.toLocaleString(),
        gameEnd: endGame.toLocaleString(),
        winField: {"field": [[null, null, null, null, null, null, null], [null, null, null, null, null, null, null], ["x", null, null, null, null, null, null], ["x", null, "o", null, null, null, null], ["x", null, "o", null, null, null, null], ["x", null, "o", null, null, null, null]], "gameOver": [[[3, 0], [4, 0], [5, 0], [2, 0]], ["x"]]}
      })
    }
    return games
  };

  for (let j = 0; j < 20; j++) {
    let arrayGames = addGames(10000, '2022-05-01T00:00:00.000Z', '2022-11-29T00:00:00.000Z', 5, 205);
    await knex('games').insert(arrayGames);
  }
  for (let j = 0; j < 60; j++) {
    let arrayGames = addGames(10000, '2023-02-01T00:00:00.000Z', '2023-02-27T00:00:00.000Z', 206, 405);
    await knex('games').insert(arrayGames);
  }
  for (let j = 0; j < 20; j++) {
    let arrayGames = addGames(10000, '2022-05-01T00:00:00.000Z', '2023-04-27T00:00:00.000Z', 406, 605);
    await knex('games').insert(arrayGames);
  }
  for (let j = 0; j < 50; j++) {
    let arrayGames = addGames(10000, '2022-05-01T00:00:00.000Z', '2023-05-27T00:00:00.000Z', 206, 405);
    await knex('games').insert(arrayGames);
  }
  for (let j = 0; j < 50; j++) {
    let arrayGames = addGames(10000, '2022-05-01 00:00:00', '2023-05-27 00:00:00', 606, 2005);
    await knex('games').insert(arrayGames);
  }
};


