require('dotenv').config()
process.env.TZ = 'Etc/Universal'
const express = require('express');
const PORT = 3001
const app = express();
const cors = require('cors')
const router = require('./routes/index')
const errorHandler = require('./middleware/errorHandlingMiddleware');
const { getUser, getOnlineUsers, getRoomId, getRoom, endGame, sendInChat, getChat, getLastGame } = require('./helpers/db');
const { calculateWinners } = require('./helpers/gameHelper');

const http = require("http").createServer(app);
const io = require("socket.io")(http, {
    cors: {
      origin: "*",
    },
});

let users = [];
let usersOnline = [];
let usersBlocked = [];
let rooms = [];

io.on("connection", (socket) => {
    users.push(socket.id)
    console.log(`⚡: ${socket.id} user just connected!`);
    socket.on('setUsersOnline', async (data) => {
        const user = await getUser(data)
        socket.join(user[0].id+data);
        if (usersOnline.find(x =>x.socketId === user[0].id)){
            const index = usersOnline.findIndex(x => x.socketId === user[0].id)
            usersOnline[index].socketIO = socket.id
        } else {
            usersOnline.push({
                socketId: user[0].id,
                login: data,
                lastGame: user[0].lastGame,
                status: 0,
                socketIO: socket.id
            })
        }
        const newUsersOnline = await getOnlineUsers(usersOnline)
        socket.broadcast.emit("updateUsersOnline", newUsersOnline);
    })

    socket.on('getUsersOnline', async (userId) => {
        
        const newUsersOnline = await getOnlineUsers(usersOnline)
        socket.emit('getUsersOnline', newUsersOnline)
        
    })

    socket.on('disconUsersOnline', async (userId) => {
        usersOnline = usersOnline.filter((user) => user.socketId !== userId)
        const newUsersOnline = await getOnlineUsers(usersOnline)
        socket.broadcast.emit("updateUsersOnline", newUsersOnline);
    })

    socket.on("inviteUser_server", async (userReqIdSocket, userRes, userReqLogin) => {
        let portfolio_user_req = await getUser(userReqLogin);
        portfolio_user_req[0].socketId = userReqIdSocket;
        socket.to(userRes.socketId+userRes.login).emit('requestInvite_client', userReqIdSocket, portfolio_user_req)
    })

    socket.on("responseInvite_server", async ( res, userReqIdSocket, userResIdSocket, userResLogin, portfolio_user_req ) => {
        if (res) {
            let portfolio_user_res = await getUser(userResLogin);
            portfolio_user_res[0].socketId = userResIdSocket;
            
            const arrPlayers = [portfolio_user_req, portfolio_user_res];
            arrPlayers[0][0].roleInGame = "x"
            arrPlayers[1][0].roleInGame = "o"
            let idRoom = await getRoomId(portfolio_user_req[0].socketId, portfolio_user_res[0].socketId)
            let dataRoom = await getRoom(idRoom)
            const room = {
                id: idRoom,
                players: arrPlayers,
                mode: 'x',
                field: [[null, null, null, null, null, null, null], 
                    [null, null, null, null, null, null, null], 
                    [null, null, null, null, null, null, null], 
                    [null, null, null, null, null, null, null], 
                    [null, null, null, null, null, null, null], 
                    [null, null, null, null, null, null, null]],
                capacity: 10,
                chat: [],
            };
            rooms.push(room);
            console.log("Room created: " + room.id);
            socket.emit('join_room_client', room, arrPlayers, arrPlayers[1][0].roleInGame, dataRoom.gameBegin)
            socket.broadcast.to(userReqIdSocket+portfolio_user_req[0].login).emit('join_room_client', room, arrPlayers, arrPlayers[0][0].roleInGame, dataRoom.gameBegin)
        } else {
            socket.broadcast.to(userReqIdSocket+portfolio_user_req[0].login).emit('responseInvite_client', {res, userResIdSocket})
        }
    });

    socket.on("join_room_server", async (roomId, userid, userLogin) => {
        socket.leave(userid+userLogin)
        socket.join(roomId); 
        console.log(`user ${socket.id} joined room: ${roomId}`);
        usersOnline.forEach((user)  => {
            if (user.socketId === userid) {
                user.status = 2;
            }
        })
        const newUsersOnline = await getOnlineUsers(usersOnline)
        socket.broadcast.emit("updateUsersOnline", newUsersOnline);
        socket.emit('start_game')
    });

    socket.on("move_player_server", async (i,j, roleInGame, idRoom) => {
        const nowRoom = rooms.find(x => x.id === idRoom)
        
        if (roleInGame === nowRoom.mode) {
            let addGame = true
            let count = 0
            for (let q = 0; q < 6; q++){
                if (nowRoom.field[q][j] !== null) {
                    count++
                } 
            }

            if (count !== 6){
                nowRoom.field[5-count][j] = roleInGame
                const winner = calculateWinners(nowRoom.field, roleInGame,5-count, j)
            
                if (winner !== false){
                    addGame = await endGame(winner, idRoom, roleInGame, nowRoom.players, nowRoom.field)
                    rooms = rooms.filter((room) => room.id !== idRoom)
                }

                if(addGame){
                    roleInGame === 'x'? nowRoom.mode = 'o' : nowRoom.mode = 'x'
                }
                socket.to(idRoom).emit("move_player_client", winner, nowRoom);
                socket.emit("move_player_client", winner, nowRoom)
            } 
            
        } else {
            socket.emit("not_your_move")
        }
        
    })

    socket.on("go_in_my_room", async (nowRoom, user) =>{
        socket.leave(nowRoom.id)
        socket.join(user.id+user.login)
        lastGame = await getRoom(nowRoom.id)
        const changeDataUser = usersOnline.find(x => x.socketId === user.id)
        changeDataUser.status = 0
        if (lastGame.length !==0){
            changeDataUser.lastGame =lastGame.gameEnd
        }
        

        const newUsersOnline = await getOnlineUsers(usersOnline)
        socket.broadcast.emit("updateUsersOnline", newUsersOnline);
    })

    socket.on("message", async (payload) => {
        await sendInChat(payload)
        const chat = await getChat(payload.gameRoom.id)
        io.to(payload.gameRoom.id).emit("chat", chat);
    });
    
    socket.on('check_last_game', async(userId, userLogin) =>{
        const dataLastGame = await getLastGame(userId)
        if (dataLastGame.result && dataLastGame.noActiveGame){
            socket.leave(userId+userLogin)
            const indexRoom = rooms.find(x => x.id === dataLastGame.data.id);
            socket.join(indexRoom.id)
            usersOnline.forEach((user)  => {
                if (user.socketId === userId) {
                    user.status = 2;
                }
            })
            const chat = await getChat(indexRoom.id)
            const newUsersOnline = await getOnlineUsers(usersOnline)
            socket.broadcast.emit("updateUsersOnline", newUsersOnline)
            socket.emit('reconnecting', indexRoom, dataLastGame.data)
            io.to(indexRoom.id).emit("chat", chat);
        }

        if (dataLastGame.result === false && dataLastGame.noActiveGame){
            const playersArr = [dataLastGame.userX, dataLastGame.userO]
            const time = [dataLastGame.closeGame.gameBegin, dataLastGame.closeGame.gameEnd] 
            const winComb = calculateWinners(dataLastGame.closeGame.field)
            socket.emit("loadLastGame", playersArr, time, dataLastGame.closeGame.winField, dataLastGame.messages)
        }

    })

    socket.on('kick', (id) => {
        const block = usersOnline.find(x=> x.socketId === id)
        if (block) {
            socket.to(block.socketIO).emit('kick helper');
        }
    })

    socket.on('sendEmoji_server', (roleInGame, room, smile) => {
        socket.to(room.id).emit("sendEmoji_client", roleInGame, smile);
        socket.emit("sendEmoji_client", roleInGame, smile)
    })
    
    
    socket.on('disconnect', async ()=>{
        usersOnline = usersOnline.filter(x => x.socketIO !== socket.id)
        socket.broadcast.emit("updateUsersOnline", usersOnline)
        console.log(socket.id + ' Disconnecting')
    })
    
})

app.use(cors())
app.use(express.urlencoded());
app.use(express.json());
app.use(router);

app.use(errorHandler)

http.listen(PORT);