const knexConfig = require('../DB/knexfile')
const knex = require('knex')(knexConfig.development)


module.exports.getOnlineUsers = async function (users_online) {
    let allUsers 
    await knex('users').join('accounts', 'users.id', '=', 'accounts.idUser').where('statusActive', true)
    .select()
    .then(users =>{
        allUsers = users
    })
    .catch(err => console.log(err))
    if (allUsers && users_online) {
        const myArrayFiltered = allUsers.filter((el) => {
            return users_online.some((f) => {
              return f.login === el.login
            });
        });
        
        const arrayWhisIdSocket = myArrayFiltered.map((y)=> Object.assign(y, users_online.find((x)=> x.login === y.login)))
        return arrayWhisIdSocket
    }

    return allUsers
}


module.exports.getUser = async function (login) {
    let portfolio_user;
    await knex('users').join('accounts', 'users.id', '=', 'accounts.idUser').where('login',login).select()
    .then(user => {
        portfolio_user = user
    }).catch(err => console.log(err));

    const dateLastGame = await knex('games')
    .where('games.idPlayer1','=',portfolio_user[0].id)
    .orWhere('games.idPlayer2','=',portfolio_user[0].id)
    .limit(1)
    .orderBy('games.id', 'DESC').select()
    
    if (dateLastGame.length !== 0){
        portfolio_user[0].lastGame = dateLastGame[0].gameEnd
    } else {
        portfolio_user[0].lastGame = null
    }
    
    return portfolio_user;

}

module.exports.getRoomId = async function (idX, idO) {
    let now = new Date().toISOString()
    let idRoom;
    await knex('games')
    .insert([{idPlayer1: idX, idPlayer2: idO, gameResult: null, gameBegin: now, gameEnd: null}])
    .returning('id')
    .into('games')
    .then(id => {
        idRoom = id[0].id
    }).catch(err => console.log(err));

    return idRoom;

}

module.exports.getRoom = async function (idRoom) {
    let DataRoom
    await knex('games').where('id', idRoom)
    .select()
    .then(data =>{
        DataRoom = data[0]
    }).catch(err => console.log(err))
    
    return DataRoom
}

module.exports.endGame = async function (winner, idRoom, roleInGame, players, field) {
    let roleBool
    if(roleInGame === 'x'){
        roleBool = true
    } else {
        roleBool = false
    }

    if (winner === 'end'){
        await knex('games').where('id', idRoom)
        .update({gameEnd: new Date().toISOString(), winField: JSON.stringify({field:field, gameOver:winner})})
        .catch(err => console.log(err))

        const addGameInMonthXDraw = await knex('counters').where('user_id',players[0][0].id)
            .andWhere(knex.raw(`month = date_trunc('month', now())`))
            .andWhere('symbol', 'x')
            .update({draws_cnt: knex.raw('?? + 1', ['draws_cnt'])})
            .returning('draws_cnt')

        const addGameInMonthODraw = await knex('counters').where('user_id',players[1][0].id)
            .andWhere(knex.raw(`month = date_trunc('month', now())`))
            .andWhere('symbol', 'o')
            .update({draws_cnt: knex.raw('?? + 1', ['draws_cnt'])})
            .returning('draws_cnt')
        if (addGameInMonthXDraw.length === 0){
            await knex('counters')
            .insert([{user_id: players[0][0].id, symbol: 'x', month: knex.raw(`date_trunc('month', now())`), wins_cnt: 0, loses_cnt: 0, draws_cnt: 1}])
        }

        if (addGameInMonthODraw.length === 0){
            await knex('counters')
            .insert([{user_id: players[1][0].id, symbol: 'o', month: knex.raw(`date_trunc('month', now())`), wins_cnt: 0, loses_cnt: 0, draws_cnt: 1}])
        }

        return false
    }
    if (winner){
        await knex('games').where('id', idRoom)
        .update({gameResult: roleBool, gameEnd: new Date().toISOString(), winField: JSON.stringify({field:field, gameOver:winner})})
        .catch(err => console.log(err))
        if (roleBool) {
            const addGameInMonthXWin = await knex('counters').where('user_id',players[0][0].id)
            .andWhere(knex.raw(`month = date_trunc('month', now())`))
            .andWhere('symbol', 'x')
            .update({wins_cnt: knex.raw('?? + 1', ['wins_cnt'])})
            .returning('wins_cnt')

            const addGameInMonthOLose = await knex('counters').where('user_id',players[1][0].id)
            .andWhere(knex.raw(`month = date_trunc('month', now())`))
            .andWhere('symbol', 'o')
            .update({loses_cnt: knex.raw('?? + 1', ['loses_cnt'])})
            .returning('loses_cnt')

            if (addGameInMonthXWin.length === 0){
                await knex('counters')
                .insert([{user_id: players[0][0].id, symbol: 'x', month: knex.raw(`date_trunc('month', now())`), wins_cnt: 1, loses_cnt: 0, draws_cnt: 0}])
                .catch(err => console.log(err))
            }

            if (addGameInMonthOLose.length === 0){
                await knex('counters')
                .insert([{user_id: players[1][0].id, symbol: 'o', month: knex.raw(`date_trunc('month', now())`), wins_cnt: 0, loses_cnt: 1, draws_cnt: 0}])
                .catch(err => console.log(err))
            }

        } else {
            const addGameInMonthXLoses = await knex('counters').where('user_id',players[0][0].id)
            .andWhere(knex.raw(`month = date_trunc('month', now())`))
            .andWhere('symbol', 'x')
            .update({loses_cnt: knex.raw('?? + 1', ['loses_cnt'])})
            .returning('loses_cnt')

            const addGameInMonthOWin = await knex('counters').where('user_id',players[1][0].id)
            .andWhere(knex.raw(`month = date_trunc('month', now())`))
            .andWhere('symbol', 'o')
            .update({wins_cnt: knex.raw('?? + 1', ['wins_cnt'])})
            .returning('wins_cnt')

            if (addGameInMonthXLoses.length === 0){
                await knex('counters')
                .insert([{user_id: players[0][0].id, symbol: 'x', month: knex.raw(`date_trunc('month', now())`), wins_cnt: 0, loses_cnt: 1, draws_cnt: 0}])
                .catch(err => console.log(err))
            }

            if (addGameInMonthOWin.length === 0){
                await knex('counters')
                .insert([{user_id: players[1][0].id, symbol: 'o', month: knex.raw(`date_trunc('month', now())`), wins_cnt: 1, loses_cnt: 0, draws_cnt: 0}])
                .catch(err => console.log(err))
            }
        }
        


        return false
    }
}

module.exports.sendInChat = async function (payload) {
    await knex('messenger')
    .insert([{idGame: payload.gameRoom.id, idUser: payload.userId, message: payload.message, createdMesAt: payload.time, role: payload.roleInGame}])
    .catch(err => console.log(err));
}

module.exports.getChat = async function (roomId) {
    let chat
    await knex('users').join('messenger', 'users.id', '=', 'messenger.idUser').where('idGame', roomId)
    .select('firstName', 'surName', 'idUser', 'message', 'createdMesAt', 'role')
    .orderBy('createdMesAt')
    .then(messages =>{
        chat = messages
    }).catch(err => console.log(err))
    return chat
}

module.exports.getLastGame = async function (userId) {
    let idLastGameX
    let idLastGameO
    let noActiveGame = false
    await knex('games').where('idPlayer1', userId).andWhere('gameEnd', null).select()
    .then(idGame =>{
        idLastGameX = idGame
    }).catch(err => console.log(err))
    
    await knex('games').where('idPlayer2', userId).andWhere('gameEnd', null).select()
    .then(idGame =>{
        idLastGameO = idGame
    }).catch(err => console.log(err))
    
    if(idLastGameX[0]) {
        idLastGameX[0].roleInGame = "x"
        return {data:idLastGameX[0], result: true, noActiveGame: !noActiveGame}
    }
    if(idLastGameO[0]) {
        idLastGameO[0].roleInGame = "o"
        return {data:idLastGameO[0], result: true, noActiveGame: !noActiveGame}
    }

    const closeGame = await knex('games').select()
    .where('idPlayer1', userId).orWhere('idPlayer2', userId)
    .orderBy('id', 'desc', 'first')

    if (closeGame.length !== 0){
        const userX = await knex('users').where('id', closeGame[0].idPlayer1)
        const userO = await knex('users').where('id', closeGame[0].idPlayer2)
        const messages = await knex('messenger').join('users', 'users.id', '=', 'messenger.idUser')
        .select('messenger.idGame AS idGame', 'messenger.idUser AS idUser', 'messenger.message AS message', 
        'messenger.role AS role', 'messenger.createdMesAt AS createdMesAt', 'users.firstName AS firstName',
        'users.surName AS surName').where('idGame', closeGame[0].id).orderBy('createdMesAt')

        return {closeGame:closeGame[0], userX:userX, userO:userO, result: false, noActiveGame: !noActiveGame, messages: messages}
    }

    return {result: true, noActiveGame: noActiveGame}
}