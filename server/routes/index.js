const express = require('express')
const router = express.Router()
const auth = require('./auth')
const rating = require('./rating')
const active = require('./active')
const history = require('./history')
const listOfPlayers = require('./listOfPlayers')



router.use('/user',auth)
router.use('/rating', rating)
router.use('/active', active)
router.use('/history', history)
router.use('/listOfPlayers', listOfPlayers)

module.exports = router