const express = require('express');
const activeController = require('../controllers/activeController');
const router = express.Router();

router.get('/', activeController.getActivePlayers)


module.exports = router