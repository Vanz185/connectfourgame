const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/auth', authMiddleware, userController.check)
router.post('/login', userController.login)
router.post('/registration', userController.addPlayer)


module.exports = router;