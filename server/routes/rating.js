const express = require('express');
const ratingController = require('../controllers/ratingController');
const router = express.Router();
const authMiddleware = require('../middleware/authMiddleware');


router.get('/',authMiddleware, ratingController.getRatingPlayers)


module.exports = router;
