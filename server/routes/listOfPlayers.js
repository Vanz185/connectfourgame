const express = require('express');
const listOfPlayersController = require('../controllers/listOfPlayersController');
const checkRoleMiddleware = require('../middleware/checkRoleMiddleware');
const router = express.Router();



router.get('/',checkRoleMiddleware, listOfPlayersController.getPlayers)
router.put('/edit',checkRoleMiddleware, listOfPlayersController.editActive)
router.put('/update',checkRoleMiddleware, listOfPlayersController.updatePlayer)



module.exports = router;