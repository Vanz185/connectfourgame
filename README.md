## ConnectFourGame

ConnectFourGame - это веб-приложение, в котором можно хорошо провести время за игрой "Четыре в ряд"

## Установка и запуск

    Клиентская часть приложения разработана на React. URL: https://localhost:3000/. Для запуска выполните команду $ npm start.

    Серверная часть приложения разработана с использованием NodeJS и Express. URL: https://localhost:3001/. Для запуска выполните команду $ npm run dev.



    


