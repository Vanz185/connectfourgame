import {$authHost} from "./index"


export const getRatingPlayers = async (limit, page, filter) => {
  const {data} = await $authHost.get('/rating',{
      params:{
        limit: limit,
        page: page,
        filter: filter
      }
    });
  return data
};