import {$authHost} from "./index"


export const getListOfPLayers = async (limit, page, id, filter) => {
  const {data} = await $authHost.get('/listOfPlayers',{
      params:{
        limit: limit,
        page: page,
        id:id,
        filter:filter
      }
    });
  return data
};

export const updateListOfPLayers = async (id, editData) => {
    const {data} = await $authHost.put('/listOfPlayers/update',{ id, editData});
    return data
};

export const editActiveListOfPLayers = async (id, reverseStatus) => {
  const {data} = await $authHost.put('/listOfPlayers/edit',{ id, reverseStatus});
  return data
};
