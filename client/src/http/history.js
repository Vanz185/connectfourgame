import {$authHost} from "./index"


export const getHistory = async (limit, page, id, startDate, endDate) => {
  const {data} = await $authHost.get('/history', {
      params:{
        limit: limit,
        page: page,
        id: id,
        startDate: startDate,
        endDate: endDate
      }
    });
  return data
};