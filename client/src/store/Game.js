import { makeAutoObservable } from "mobx";

class Game {
    
    mode = 'x';
    field = [[null, null, null, null, null, null, null], 
        [null, null, null, null, null, null, null], 
        [null, null, null, null, null, null, null], 
        [null, null, null, null, null, null, null], 
        [null, null, null, null, null, null, null], 
        [null, null, null, null, null, null, null]];
    gameOver = false;
    room = false
    roleInGame = false
    playersRoom = false

    constructor() {
        makeAutoObservable(this);
    };

    setPlayersRoom (value){
        this.playersRoom = value
    }

    setMode (value) {
        this.mode = value
    }

    setField (value){
        
        this.field = value
    }

    setWinComb (value){
        this.gameOver = value
    }

    setRoom (value){
        this.room = value
    }

    setRoleInGame (value){
        this.roleInGame = value
    }
    
    

    styleBack (i,j) {
        if (this.gameOver !== false && this.gameOver !=='end') {                                   
            if (this.gameOver[0].filter(mas => JSON.stringify(mas) === JSON.stringify([i,j])).length !== 0) {     
              if (this.gameOver[1][0] === 'x'){
                return 'winSquareX';
              }     
              else{
                return 'winSquareO';
              }
                
            }
          }
        return 'cell';
    };


};

export default Game = new Game();