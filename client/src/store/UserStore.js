import { makeAutoObservable } from "mobx";
import io from 'socket.io-client'

class UserStore {
    
    constructor() {
        this._isAuth = false
        this._isAdmin = false
        this._user = {}
        this._socket = io('http://localhost:3001')
        makeAutoObservable(this)
        
    };

    setIsAuth (bool) {
        this._isAuth = bool
    }

    setIsAdmin (bool) {
        this._isAdmin = bool
    }

    setUser (user){
        this._user = user
    }

    get isAuth() {
        return this._isAuth
    }

    get isAdmin() {
        return this._isAdmin
    }

    get user() {
        return this._user
    }

    get socket(){
        return this._socket;
    }
   
};

export default UserStore = new UserStore();