import { makeAutoObservable } from "mobx";

class Emoji {
    
    isVisibleX = false
    isVisibleO = false
    smileX = false
    smileO = false
    block = true

    constructor() {
        makeAutoObservable(this);
    };

    setIsVisibleX (value){
        this.isVisibleX = value
    }
    setIsVisibleO (value){
        this.isVisibleO = value
    }

    setSmileX (value) {
        this.smileX = value
    }
    setSmileO (value) {
        this.smileO = value
    }

    setBlock (value){
        this.block = value
    }
    
    

    


};

export default Emoji = new Emoji();