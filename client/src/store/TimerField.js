import { action, computed, makeAutoObservable, observable } from "mobx";
import { makePersistable, stopPersisting } from "mobx-persist-store";
import Game from "./Game";


class TimerField {
    startGame = 0
    time = 0
    constructor() {
        makeAutoObservable(this);
    };
    setTime() {
      this.time = Date.now() - this.startGame;
    };

    setStartGame(date) {
      this.startGame = date;
    };

    setTimeEndGame (value){
      this.time = Date.parse(value[1])-Date.parse(value[0])
    }

};

export default TimerField = new TimerField();