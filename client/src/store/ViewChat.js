import { makeAutoObservable } from "mobx";

class ViewChat {
    
    isVisible = false

    constructor() {
        makeAutoObservable(this);
    };

    setIsVisible (value){
        this.isVisible = value
    }
};

export default ViewChat = new ViewChat();