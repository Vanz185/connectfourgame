import { makeAutoObservable } from "mobx";

class ViewEmoji {
    
    isVisible = false

    constructor() {
        makeAutoObservable(this);
    };

    setIsVisible (value){
        this.isVisible = value
    }
};

export default ViewEmoji = new ViewEmoji();