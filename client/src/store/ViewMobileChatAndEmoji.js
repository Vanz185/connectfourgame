import { makeAutoObservable } from "mobx";

class ViewMobileChatAndEmoji {
    
    blockbtnChat = true
    blockbtnEmotion = true

    constructor() {
        makeAutoObservable(this);
    };

    setBlockbtnChat (value){
        this.blockbtnChat = value
    }
    setBlockbtnEmotion (value){
        this.blockbtnEmotion = value
    }
};

export default ViewMobileChatAndEmoji = new ViewMobileChatAndEmoji();