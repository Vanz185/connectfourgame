import { action, computed, makeAutoObservable, observable } from "mobx";


class ChatStore {
    
    listMessage = []
    constructor() {
        makeAutoObservable(this);
    };

    
    setListMessage(value) {
        this.listMessage = value
    }
    

    resetListMessage() {
        this.listMessage = []
    }
    

};

export default ChatStore = new ChatStore();