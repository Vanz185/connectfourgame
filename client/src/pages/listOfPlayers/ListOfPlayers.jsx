import React, { useRef, useState } from "react";
import PlayersList from "../../components/listOfPlayers/PlayersList";
import './ListOfPlayersStyle.scss'
import './ListOfPlayersAdaptive.scss';
import { editActiveListOfPLayers, getListOfPLayers, updateListOfPLayers } from "../../http/listOfPlayers";
import { useEffect } from "react";
import Loading from "../../components/Loading/Loading";
import UserStore from "../../store/UserStore";
import EditPlayerModal from "../../components/modals/editPlayerModal/EditPlayerModal";
import InEditPlayerModal from "../../components/modals/editPlayerModal/InEditPlayerModal";
import MyInput from "../../UI/input/MyInput";
import clearСross from '../../UI/icons/exitModal.svg'
const ListOfPlayers = () => {
    const [loading, setLoading] = useState(true)
    const [modal, setModal] = useState(false);
    const [player, setPlayer] = useState(false)
    const [totalCount, setTotalCount] = useState(0)
    const [pages, setPages] = useState(1)
    const [playersList, setPlayersList] = useState([])
    const socket = UserStore.socket
    const [filter, setFilter] = useState('')
    const [changeFilter, setChangeFilter] = useState(false)
    
    const lastElement = useRef()
    const observer = useRef()
    
    
    useEffect(() => {
        
        if (!changeFilter){
            getListOfPLayers(20, pages, UserStore.user.id, filter).then(data => {
                setTotalCount(data.totalCount[0].count)
                setPlayersList([...playersList, ...data.listOfPLayers])
            }).catch(e => console.log(e)).finally(() => {setLoading(false); setChangeFilter(true)})
        }
    }, [changeFilter])

    useEffect(()=>{
        
        if (changeFilter){
            const Timer = setTimeout(()=>{
                setLoading(true)
                setPlayersList([])
                setPages(1)
                setTotalCount(0)
                setChangeFilter(false)
            }, 1000)
            
            return() => {clearTimeout(Timer)}
            
        }
    }, [filter])

    useEffect(()=>{
        if(loading) return;
        if(observer.current) observer.current.disconnect();
        let callback = function(entries, observer){
            if(entries[0].isIntersecting && (pages * 20) < totalCount){
                setLoading(true)
                setPages(pages + 1)
                setChangeFilter(false)
                
            }
        }
        observer.current = new IntersectionObserver(callback);
        observer.current.observe(lastElement.current)
    }, [loading])

    const editActive = (index, reverseStatus, id) =>{
        
        editActiveListOfPLayers(id, reverseStatus).then(data => {
            socket.emit('kick', id)
            const edit = playersList.map((row, i) => {
                if (i === index) {
                    return {...row, statusActive: data.statusActive, updatedAt: new Date(data.updatedAt)}
                } else
                    return row
            })
        setPlayersList(edit)})
        .catch((e) =>console.log(e))
        
    }

    const exit = () =>{
        setModal(false)
        setPlayer(false)
    }



    const editPlayer = (editData) =>{
        
        updateListOfPLayers(editData.id, editData.dataPlayer).then(data =>{
            const update = playersList.map((row, i) => {
                if (i === editData.index) {
                    return {...row, surName: data.surName, 
                    firstName: data.firstName, 
                    secondName: data.secondName,
                    age: data.age,
                    gender:data.gender, 
                    updatedAt: data.updatedAt}
                } else
                    return row
            })
            setPlayersList(update)
            setModal(false)
            setPlayer(false)
        }).catch((e)=> console.log(e))
    }
    
    return (  
        <>
        <div className="main-listOfPlayers">
            <div className="listOfPlayers-panel">
                <div className="header-listOfPlayers">
                    <h1>Список игроков</h1>
                    <div className="input-case">
                        <MyInput
                            value={filter}
                            onChange={e => setFilter(e.target.value)}
                            placeholder="Поиск по ФИО"
                        />
                        <img src={clearСross} alt="" onClick={() => setFilter('')}/>
                    </div>
                </div>
                <div className="list-listPlayers">
                    <ul className='playerList playerList-up'>
                        <li className='name-playerList'>ФИО</li>
                        <li className='age-playerList'>Возраст</li>
                        <li className='gender-playerList'>Пол</li>
                        <li className='status-playerList'>Статус</li>
                        <li className='dateCreate-playerList'>Создан</li>
                        <li className='dateEdit-playerList'>Изменен</li>
                    </ul>
                    <PlayersList playersList = {playersList} editActive={editActive} setModal = {setModal} setPlayer={setPlayer}/>
                    <div ref={lastElement} style={{height:1}}></div>
                    {loading &&
                        <div style={{alignSelf: 'center'}}><Loading/></div>
                    }
                </div>
            </div>
            {modal &&
                <EditPlayerModal visible={modal} setVisible={setModal} setPlayer={setPlayer}>
                    <InEditPlayerModal editPlayer={editPlayer} exit={exit} player={player}/>
                </EditPlayerModal>
            }
            

        </div>
        </>
    );
}
 
export default ListOfPlayers;