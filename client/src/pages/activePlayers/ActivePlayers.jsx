import React, { useEffect, useMemo, useState } from "react";
import ActivePlayersList from "../../components/activePlayer/ActivePlayersList";
import './ActivePlayersStyle.scss'
import './ActivePlayersAdaptive.scss'
import UserStore from "../../store/UserStore";
import jwt_decode from 'jwt-decode'
import { useMatchMedia } from "../../hooks";


const ActivePLayers = () => {
    const [statusPlayers, setStatusPlayers] = useState([])
    const [checked, setChecked] = useState (false);
    const { isMobile, isTablet, isDesktop } = useMatchMedia();
    const socket = UserStore.socket
    
    useEffect(() => {
        socket.emit('getUsersOnline', UserStore.user.id);
        socket.on('getUsersOnline', (usersData) => {
            usersData = usersData.filter(x => x.socketId !== UserStore.user.id)
            setStatusPlayers(usersData)
        });
    }, [])

    useEffect(() => {
        socket.on('updateUsersOnline', (users) => {
            users = users.filter(x => x.socketId !== UserStore.user.id)
            setStatusPlayers(users) 
        });
    }, [statusPlayers])


    const filter = useMemo(()=>{
        if (checked){
            return (statusPlayers.filter(item => item.status === 0))
        
        }else{
            return statusPlayers
        }
    }, [statusPlayers, checked])

    const inviteUser = (oponent) => {
        if (localStorage.getItem('token') !== null){
            const token = localStorage.getItem('token')
            const decoded = jwt_decode(token);
            if (decoded.exp * 1000 < new Date().getTime()){
                alert('Время авторизации истекло. Перезайдите в аккаунт.')
            } else {
                socket.emit('inviteUser_server', UserStore.user.id, oponent, UserStore.user.login);
            }
        } else {
            alert('Время авторизации истекло. Перезайдите в аккаунт.')
        }
    };
    
    return ( 
        <div className='activePlayersTable'>
            <div className='activePlayers-container'>
                <div className='activePlayers-header'>
                    <h1 className='title-block'>Активные игроки</h1>
                    <div className='activePlayers-header-container'>
                        <p>Только свободные</p>
                        <label className='switch'>
                            <input 
                                checked={checked} 
                                onChange={e =>setChecked(checked ? false : true)}
                                type="checkbox" 
                                className="switch-input"
                            />
                            <span className="switch-slider"></span>
                        </label>
                    </div>
                </div>    
                <div className="list-active">
                        <ul className='player-active player-active-up'>
                            <li className='name-active'>ФИО</li>
                            <li className='lastgame-active'>Последняя игра</li>
                            <li className='status-active'>Статус</li>
                            {isMobile &&<li className='invite-active'>Пригласить</li>}
                        </ul>
                        <ActivePlayersList statusPlayers = {filter} inviteUser={inviteUser}/>
                </div>
            </div>
        </div>
    );
}
 
export default ActivePLayers;