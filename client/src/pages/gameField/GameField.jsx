import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';
import './GameFieldStyle.scss'
import './GameFieldAdaptive.scss'
import Field from "../../components/gameField/Field";
import { observer } from 'mobx-react-lite'
import Game from "../../store/Game";
import Chat from "../../components/chat/Chat";
import WinModal from "../../components/modals/winModal/WinModal";
import InWinModal from "../../components/modals/winModal/InWinModal";
import UserStore from "../../store/UserStore";
import Emotions from "../../components/gameField/emotions/Emotions";
import { useMatchMedia, useMatchMediaField } from "../../hooks";
import GamePanel from "../../components/gameField/GamePanel";
import Timer from "../../components/timer/Timer";

const GameField = () => {
    const [modal, setModal] = useState(false);
    const navigate = useNavigate();
    const { isMobileField, isTabletField, isDesktopField } = useMatchMediaField()
    const { isMobile, isTablet, isDesktop } = useMatchMedia()
    const socket = UserStore.socket

    const startNewGame = () => {
        let opponent;
        Game.playersRoom.forEach((player) => {
            if (player[0].login !== UserStore.user.login) {
             opponent = player[0];
            }
        });
        
        socket.emit('inviteUser_server', UserStore.user.id, opponent, UserStore.user.login);
    }

    const outGame =() =>{
        Game.setRoleInGame(false)
        setModal(false)
        navigate('/activePlayers')
    }

    const handleClick = (i,j) => {

        if (Game.playersRoom && Game.room){
            socket.emit('move_player_server', i, j, Game.roleInGame, Game.room.id)
        }
            
    };

    useEffect(()=>{
        socket.on('move_player_client', (winner, nowRoom)=>{
            Game.setField(nowRoom.field)
            Game.setMode(nowRoom.mode)
            Game.setWinComb(winner)
            if (winner !== false){
                socket.emit('go_in_my_room', nowRoom, UserStore.user)
                socket.removeAllListeners('requestInvite_client');
                socket.removeAllListeners('responseInvite_client');
                socket.removeAllListeners('join_room_client');
                socket.removeAllListeners('not_your_move');
                Game.setRoom(false)
            }
        })
    }, [])
    
    
    
    return ( 
        <div className="main-gameField">
            <div className="gameField-panel">
                {isDesktopField && <Emotions/>}
                {(isMobile || isTablet) &&
                    <>
                        <div className="game-panel">
                            <GamePanel/>
                        </div>
                        <div className="timer">
                            <Timer/>
                        </div>
                    </>
                }
                <div className="game-container">
                    <div className="board-panel">
                        <div className="board" >
                            <Field board = {Game.field} handleClick = {handleClick}></Field>
                        </div>
                    </div>
                </div>
                {isDesktopField && <Chat/>}
                {(isMobileField || isTabletField) &&
                    <div className="emoji-chat-panel">
                        <Emotions/>
                        <Chat/>
                    </div>
                }
            </div>
            <WinModal visible = {modal} setVisible={setModal}>
              <InWinModal startNew ={startNewGame} outGame = {outGame}/>
            </WinModal>
            
        </div>
    );
}
 
export default observer(GameField);