import React, { useEffect, useRef, useState } from "react";
import DatePicker from "react-datepicker";
import HistoryList from "../../components/history/HistoryList";
import './HistoryTableStyle.scss'
import './HistoryTableAdaptive.scss'
import FieldModal from "../../components/modals/fieldModal/FieldModal";
import InFieldModal from "../../components/modals/fieldModal/InFieldModal";
import Loading from '../../components/Loading/Loading'
import { getHistory } from "../../http/history";
import UserStore from "../../store/UserStore";
import "react-datepicker/dist/react-datepicker.css";
const HistoryTable = () => {
    const [modal, setModal] = useState(false);
    const [dataField, setDataField] = useState({field:[]});
    const [playersHistory, setPlayersHistory] = useState([])
    const [loading, setLoading] = useState(true)
    const [pages, setPages] = useState(1)
    const [totalCount, setTotalCount] = useState(0)
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [changeFilter, setChangeFilter] = useState(false)
    const lastElement = useRef()
    const observer = useRef()
    
    useEffect(() => {
        
        if (!changeFilter){
            getHistory(20, pages, UserStore.user.id, startDate, endDate).then(his => {
                setTotalCount(his.totalCount[0].count)
                setPlayersHistory([...playersHistory, ...his.historyPlayers])
            }).catch(e => console.log(e)).finally(() => {setLoading(false); setChangeFilter(true)})
        }
    }, [changeFilter])

    useEffect(()=>{
        
        if (changeFilter){
            const Timer = setTimeout(()=>{
                setLoading(true)
                setPlayersHistory([])
                setPages(1)
                setTotalCount(0)
                setChangeFilter(false)
            }, 1000)
            
            return() => {clearTimeout(Timer)}
            
        }
    }, [startDate, endDate])

    useEffect(()=>{
        if(loading) return;
        if(observer.current) observer.current.disconnect();
        let callback = function(entries, observer){
            if(entries[0].isIntersecting && (pages * 20) < totalCount){
                setLoading(true)
                setPages(pages + 1)
                setChangeFilter(false)
            }
        }
        observer.current = new IntersectionObserver(callback);
        observer.current.observe(lastElement.current)
    }, [loading])
    
    
    return ( 
        <>
        
        <div className='main-history'>
            <div className='history-panel'>
                <div className="history-header">
                    <h1>История игр</h1>
                    <div className="date-case">
                        <div className="date-Before">
                            <p>
                                {'C '}
                            </p>
                            <input 
                                type="date"
                                onKeyDown={(e) => {
                                    e.preventDefault();
                                }}
                                onChange={e => setStartDate(e.target.value)}
                                className="datePicker"/>
                        </div>

                        <div className="date-After">
                            <p>
                                {'По '}
                            </p>
                            <input 
                                type="date"
                                onKeyDown={(e) => {
                                    e.preventDefault();
                                }}
                                onChange={e => setEndDate(e.target.value)}
                                className="datePicker"/>
                        </div>
                    </div>
                </div>
                
                <div className='list-history'>
                    <ul className='players-history players-history-up'>
                        <li className='info-players-game'>Игроки</li>
                        <li className='date-game'>Дата</li>
                        <li className='time-game'>Время игры</li>
                        <li className='game-field'>Игровое поле</li>
                    </ul>
                    <HistoryList playersHistory ={playersHistory} setDataField ={setDataField} setModal = {setModal}/>
                    <div ref={lastElement} style={{height:1}}></div>
                    {loading &&
                        <div style={{alignSelf: 'center'}}><Loading/></div>
                    }
                </div>
            </div>
            <FieldModal visible = {modal} setVisible={setModal}>
                <InFieldModal dataField = {dataField} setVisible={setModal}/>
            </FieldModal>
        </div>
        </>
    );
}
 
export default HistoryTable;