import React, { useEffect, useRef, useState } from "react";
import RatingPlayersList from "../../components/rating/RatingPlayersList";
import './RatingTableStyle.scss'
import './RatingTableAdaptive.scss'
import { getRatingPlayers } from "../../http/ratingAPI";
import Loading from "../../components/Loading/Loading";
import MyInput from "../../UI/input/MyInput";
import clearСross from '../../UI/icons/exitModal.svg'

const RatingTable = () => {
    const [rating, setRating] = useState([])
    const [loading, setLoading] = useState(true)
    const [pages, setPages] = useState(1)
    const [totalCount, setTotalCount] = useState(0)
    const [filter, setFilter] = useState('')
    const [changeFilter, setChangeFilter] = useState(false)
    const lastElement = useRef()

    const observer = useRef()

    useEffect(() => {
        if (!changeFilter){
            getRatingPlayers(20, pages, filter).then(data => {
                setTotalCount(data.totalCount[0].count)
                setRating([...rating, ...data.ratingPlayers])
            }).catch(e => console.log(e)).finally(() => {setLoading(false); setChangeFilter(true)})
        }
    }, [changeFilter])

    useEffect(()=>{
        
        if (changeFilter){
            const Timer = setTimeout(()=>{
                setLoading(true)
                setRating([])
                setPages(1)
                setTotalCount(0)
                setChangeFilter(false)
            }, 1000)
            
            return() => {clearTimeout(Timer)}
            
        }
    }, [filter])

    useEffect(()=>{
        if(loading) return;
        if(observer.current) observer.current.disconnect();
        let callback = function(entries, observer){
            if(entries[0].isIntersecting && (pages * 20) < totalCount){
                setLoading(true)
                setPages(pages + 1)
                setChangeFilter(false)
                
            }
        }
        observer.current = new IntersectionObserver(callback);
        observer.current.observe(lastElement.current)
    }, [loading])
    
    return (  
        <>
        <div className="main-rating">
            <div className="rating-panel">
                <div className="rating-header">
                    <h1>Рейтинг игроков</h1>
                    <div className="input-case">
                        <MyInput
                            value={filter}
                            onChange={e => setFilter(e.target.value)}
                            placeholder="Поиск по ФИО"
                        />
                        <img src={clearСross} alt="" onClick={() => setFilter('')}/>
                    </div>
                </div>
                <div className="list-rating">
                    <ul className='player-rating player-rating-up'>
                        <li className='name-rating'>ФИО</li>
                        <li className='coutGames-rating'>Всего игр</li>
                        <li className='coutWin-rating'>Победы</li>
                        <li className='coutLos-rating'>Проигрыши</li>
                        <li className='procWin-rating'>Процент побед</li>
                    </ul>
                    <RatingPlayersList rating ={rating}/>
                    <div ref={lastElement} style={{height:1}}></div>
                    {loading &&
                        <div style={{alignSelf: 'center'}}><Loading/></div>
                    }
                </div>
            </div>
        </div>
        </>
    );
}
 
export default RatingTable;