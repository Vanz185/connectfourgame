import React, { useEffect, useState } from "react";
import MyInput from "../../UI/input/MyInput";
import MyButton from "../../UI/button/MyButton";
import { NavLink } from "react-router-dom";
import man from "../../UI/icons/man.svg"
import women from "../../UI/icons/women.svg"
import './RegistrationStyle.scss'
import UserStore from "../../store/UserStore";
import { addUser } from "../../http/userAPI";
import './RegistrationAdaptive.scss'

const Registration = () => {
    
    const [authDate, setAuthDate] = useState({fio: '', login:'', password:'', age: '', gender: ''})
    const [error, setError] = useState(false)
    const [disableBtn, setDisableBtn] = useState(false)
    const socket = UserStore.socket
    
    const registration = async (event) =>{
        event.preventDefault();
        try{
            let user;
            user = await addUser(authDate.fio.split(" ")[0], authDate.fio.split(" ")[1], authDate.fio.split(" ")[2], authDate.age, authDate.gender, authDate.login, authDate.password)
            UserStore.setUser(user)
            UserStore.setIsAuth(true)
            socket.emit('setUsersOnline', user.login);
        } catch (e) {
            console.log(e)
            setError(true)
        }
    }

    useEffect(()=> {
        if (authDate.fio.trim().split(/\s+/).length === 3 && authDate.login !=='' && authDate.password !=='' && authDate.age !=='' && authDate.gender !=='') setDisableBtn(false)
        else setDisableBtn(true)
        
    }, [authDate]);

    
    return ( 
        <div className="main-registr">
            <div className="registr-model">
                <h1>Регистрация</h1>
                <form onSubmit={registration}>
                    <div className="registr-data">
                        <div className="inputCase">
                            <p>ФИО</p>
                            <MyInput
                                value={authDate.fio}
                                onChange={e => setAuthDate({...authDate, fio: e.target.value})}
                                placeholder="Иванов Иван Иванович" 
                                type="text"
                                
                            />
                        </div>
                        <div className="inputCase">
                            <p>Логин</p>
                            <MyInput
                                value={authDate.login}
                                onChange={e => setAuthDate({...authDate, login: e.target.value})}
                                placeholder="Логин" 
                                type="text"
                            />
                        </div>
                        <div className="inputCase">
                            <p>Пароль</p>
                            <MyInput 
                                value={authDate.password}
                                onChange={e => setAuthDate({...authDate, password: e.target.value})}
                                type="password" 
                                placeholder="Пароль"
                            />
                        </div>
                        <div className="age-gender-set">
                            <div className="age-set">
                                <p>Возраст</p>
                                <MyInput 
                                    value={authDate.age}
                                    onChange={e => setAuthDate({...authDate, age: e.target.value})}
                                    type="text" 
                                    placeholder="0"
                                />
                            </div>
                            <div className="gender-set">
                                <p>Пол</p>
                                <div className="gender">
                                    <label className='women'>
                                        <input
                                            type="radio"
                                            name="gender"
                                            value="women"
                                            id="women"
                                            onChange={e => setAuthDate({...authDate, gender: false})}
                                        />
                                        <img src={women} alt="" />
                                    </label>
                                    <label className='man'>
                                        <input
                                            type="radio"
                                            name="gender"
                                            value="man"
                                            id="man"
                                            onChange={e => setAuthDate({...authDate, gender: true})}
                                        />
                                        <img src={man} alt="" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="registr-button">
                        <MyButton
                            disabled = {disableBtn}
                        >
                            Зарегестрироваться
                        </MyButton>
                    </div>
                    <div>Есть аккаунт? <NavLink to = '/login'> Войдите!</NavLink></div>
                </form>
            </div>
        </div> 
    );
}
 
export default Registration;