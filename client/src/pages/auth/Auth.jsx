import React, { useEffect, useState } from "react";
import MyInput from "../../UI/input/MyInput";
import MyButton from "../../UI/button/MyButton";
import './AuthStyle.scss';
import emailIcon from "../../UI/icons/emailAuth.svg"
import passwordIcon from "../../UI/icons/passwordAuth.svg"
import { NavLink } from "react-router-dom";
import UserStore from "../../store/UserStore";
import authLogo from "../../UI/icons/authLogo.svg"
import { login } from "../../http/userAPI";
import './AuthAdaptive.scss'



const Auth = () => {
    
    const [authDate, setAuthDate] = useState({login:'',password:''})
    const [error, setError] = useState(false)
    const [disableBtn, setDisableBtn] = useState(false)
    const socket = UserStore.socket
    
    
    const auth = async (event) =>{
        event.preventDefault();
        try{
            let user;
            user = await login(authDate.login, authDate.password)
            UserStore.setUser(user)
            UserStore.setIsAuth(true)
            if (user.role === 'admin'){
                UserStore.setIsAdmin(true)
            }
            socket.emit('setUsersOnline', user.login);
        } catch (e) {
            console.log(e)
            setError(true)
        }
        
        
    }

    useEffect(()=> {
        if (authDate.login !=='' && authDate.password !=='') setDisableBtn(false)
        else setDisableBtn(true)
        
    }, [authDate]);
    
    
    return ( 

        <div className="main-auth">
            <div className="auth-model">
                <div className="logoAuth"><img src={authLogo} alt=""/></div>
                <h1>Войдите в игру</h1>
                <form onSubmit={auth}>
                    <div className="auth-data">
                        <div className="login">
                            <div className="email-icon"><img src={emailIcon} alt="" /></div>
                            <div className="login-case">
                            {!error 
                            ?
                            <MyInput
                                value={authDate.login}
                                onChange={e => setAuthDate({...authDate, login: e.target.value})}
                                placeholder="Логин" 
                                type="text"
                            />
                            :
                            <input
                                value={authDate.login}
                                onChange={e => setAuthDate({...authDate, login: e.target.value})}
                                placeholder="Логин" 
                                type="text"
                                className='errorInput'
                            />
                            }
                            {error && <div className='errorInputText'>Неверный логин</div>}
                            </div>
                        </div>
                        <div className="password">
                            <div className="password-icon"><img src={passwordIcon} alt="" /></div>
                            <div className="password-case">
                                {!error 
                                ?
                                <MyInput 
                                    value={authDate.password}
                                    onChange={e => setAuthDate({...authDate, password: e.target.value})}
                                    type="password" 
                                    placeholder="Пароль"
                                />
                                :
                                <input
                                    value={authDate.password}
                                    onChange={e => setAuthDate({...authDate, password: e.target.value})}
                                    type="password" 
                                    placeholder="Пароль"
                                    className='errorInput'
                                />
                                }
                                {error && <div className='errorInputText'>Неверный пароль</div>}
                            </div>
                        </div>
                    </div>
                    <div className="auth-button">
                        <MyButton
                            disabled = {disableBtn}
                        >
                            Войти
                        </MyButton>
                    </div>
                    <div>Нет аккаунта? <NavLink to = '/registration'> Зарегистрируйся!</NavLink></div>
                </form>
            </div>
        </div>

    );
}
 
export default Auth;