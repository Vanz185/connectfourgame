import React, { createContext, useEffect, useState } from 'react';
import {BrowserRouter} from 'react-router-dom'
import AppRouter from './components/AppRouter';
import './style/App.css'
import { check } from './http/userAPI';
import UserStore from './store/UserStore';
import jwt_decode from 'jwt-decode';
import Loading from './components/Loading/Loading';
import io from 'socket.io-client'
export const AuthContext = createContext();

const socket = UserStore.socket;

function App() {
  const [loading, setLoading] = useState(true);
  
  useEffect(()=>{
    check().then((data) =>{
      UserStore.setIsAuth(true)
    }).catch(e => console.log(e)).finally(() => setLoading(false))
    
    if (localStorage.getItem('token') !== null){
      var token = localStorage.getItem('token')
      var decoded = jwt_decode(token);
      if (decoded.exp * 1000 > new Date().getTime()){
        if (decoded.role === 'admin'){
          UserStore.setIsAdmin(true)
        }
        UserStore.setUser(decoded)
        socket.emit('setUsersOnline', decoded.login);
      }
    }
    

  },[])
  
  if (loading){
    return <Loading/>
  }
  
  return (
    <AuthContext.Provider value={{
      
    }}>
        <BrowserRouter>
          <AppRouter/>
        </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default App;
