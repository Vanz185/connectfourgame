import ActivePLayers from "../pages/activePlayers/ActivePlayers";
import Auth from "../pages/auth/Auth";
import Registration from "../pages/auth/Registration";
import GameField from "../pages/gameField/GameField";
import HistoryTable from "../pages/history/HistoryTable"
import ListOfPlayers from "../pages/listOfPlayers/ListOfPlayers";
import RatingTable from "../pages/rating/RatingTable";



export const adminRoutes = [
    {path: '/gameField', element: <GameField/>, exact: true},
    {path: '/gameHistory', element: <HistoryTable/>, exact: true},
    {path: '/activePlayers', element: <ActivePLayers/>, exact: true},
    {path: '/listOfPlayers', element: <ListOfPlayers/>, exact: true},
    {path: '/ratingField', element: <RatingTable/>, exact: true},
]


export const authRoutes = [
    {path: '/gameField', element: <GameField/>, exact: true},
    {path: '/gameHistory', element: <HistoryTable/>, exact: true},
    {path: '/activePlayers', element: <ActivePLayers/>, exact: true},
    {path: '/ratingField', element: <RatingTable/>, exact: true},
]



export const publicRoutes =[
    {path: '/login', element: <Auth/>, exact: true},
    {path: '/registration', element: <Registration/>, exact: true},
]