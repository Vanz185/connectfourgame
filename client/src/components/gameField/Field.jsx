import React from "react";
import { observer } from 'mobx-react-lite'
import blueToken from '../../UI/icons/blueToken.svg'
import orangeToken from '../../UI/icons/orangeToken.svg'
import Game from "../../store/Game";
import { motion, AnimatePresence} from "framer-motion";

const Field = ({board,handleClick}) => {

    const show = {
        opacity: 1,
        scale: [0, 0.2, 0.4, 0.6, 0.8, 1],
        rotate: [0, 360],
        transition: {
            scale:{
                duration: 0.35
            },
            rotate:{
                duration: 0.35
            }
        },
    };

    return ( 
        <>
        {board.map((row, i) => (
            row.map((ch, j) =>
                <div key={j} onClick={() => handleClick(i,j)} className={`${Game.styleBack(i,j)}`}>
                    <AnimatePresence>
                        {ch &&
                            <motion.img 
                            src={ch ==='x'? blueToken: orangeToken} 
                            alt="" width='100%' 
                            height='100%'
                            animate={show} 
                            />
                            
                        }
                    </AnimatePresence>
                </div>
            ) 
        ))}
        </>
    );
}
 
export default observer(Field);