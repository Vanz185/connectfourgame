import React, { useState } from "react";
import { motion, AnimatePresence} from "framer-motion";
import blueToken from '../../UI/icons/blueToken.svg'
import orangeToken from '../../UI/icons/orangeToken.svg'
import Timer from '../timer/Timer';
import Game from '../../store/Game';
import Emoji from '../../store/Emoji';
import { observer } from 'mobx-react-lite'
import './GamePanelStyle.scss'
import './GamePanelAdaptive.scss'
import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useMatchMedia } from "../../hooks";
const GamePanel = () => {

    const [gameHeader, setGameHeader] = useState(false)
    const { isMobile, isTablet, isDesktop } = useMatchMedia()
    
    useEffect(()=>{
        if (window.location.pathname === '/gameField'){
            setGameHeader(true)
        } else {
            setGameHeader(false)
        }
        
    }, [useLocation()])
    
    const tockenForGameX = () =>{
        if (Game.room){
            if (Game.mode === 'x'){
                return 'opac1'
            } else {
                return 'opac2'
            }
        } else {
            return 'no'
        }
         
    }

    const tockenForGameO = () =>{
        if (Game.room){
            if (Game.mode === 'o'){
                return 'opac1'
            } else {
                return 'opac2'
            }
        } else {
            return 'no'
        }
         
    }

    const show = {
        opacity: 1,
        scale: [1, 2, 2, 1, 1],
        rotate: [0, 10, -10, 10, -10, 10, -10, 10, -10, 10, -10, 0],
    };

    const hide = {
        opacity: 0, 
    };

    return ( 
        <>
        {gameHeader &&
        <div className="game-panel">
            <div className="player-panel">
                <img src={blueToken} alt="" className={tockenForGameX()}/>
                <div className='player-emoji-X'>
                     <span>{Game.playersRoom ? Game.playersRoom[1][0]?.surName+' '+ Game.playersRoom[1][0]?.firstName.slice(0,1)+ '. '+Game.playersRoom[1][0]?.secondName.slice(0,1) +'.' : ''}</span>
                    <div className="sendEmoji-X">
                        <AnimatePresence>
                            {Emoji.isVisibleX &&
                                <motion.div 
                                className="box" 
                                animate={show} 
                                exit={hide}
                                transition={{duration:1.5}}
                                
                                >
                                    <img src={Emoji.smileX} alt="" />
                                </motion.div>
                            }
                        </AnimatePresence>
                    </div>
                </div>
            </div>
            {(isDesktop) && <div className="timer"><Timer/></div>}
            <div className="player-panel">
                <div className='player-emoji-O'>
                     <span>{Game.playersRoom ? Game.playersRoom[0][0]?.surName+' '+ Game.playersRoom[0][0]?.firstName.slice(0,1)+ '. '+Game.playersRoom[0][0]?.secondName.slice(0,1) +'.' : ''}</span>
                    <div className="sendEmoji-O">
                        <AnimatePresence>
                            {Emoji.isVisibleO &&
                                <motion.div 
                                className="box" 
                                animate={show} 
                                exit={hide}
                                transition={{duration:1.5}}
                                >
                                    <img src={Emoji.smileO} alt="" />
                                </motion.div>
                            }
                        </AnimatePresence>
                    </div>
                </div>
                <img src={orangeToken} alt="" className={tockenForGameO()}/>
            </div>
            
        </div>
        }
        </>
    );
}
 
export default observer(GamePanel);