import React, { useEffect, useState } from "react";
import { observer } from 'mobx-react-lite'
import { motion, AnimatePresence} from "framer-motion";
import './EmotionsStyle.scss'
import './EmotionsAdaptive.scss'
import angry from '../../../UI/emoji/angry.svg'
import cool from '../../../UI/emoji/cool.svg'
import cry from '../../../UI/emoji/cry.svg'
import devil from '../../../UI/emoji/devil.svg'
import happy from '../../../UI/emoji/happy.svg'
import laughAndCry from '../../../UI/emoji/laughAndCry.svg'
import Emoji from "../../../store/Emoji";
import Game from "../../../store/Game";
import UserStore from "../../../store/UserStore";
import arrowBack from '../../../UI/icons/arrowBack.svg'
import arrowRight from '../../../UI/icons/arrowRight.svg'
import ViewEmoji from "../../../store/ViewEmoji";
import { useMatchMediaField } from "../../../hooks";
import ViewMobileChatAndEmoji from "../../../store/ViewMobileChatAndEmoji";

const Emotions = () => {
    const socket = UserStore.socket
    const { isMobileField, isTabletField, isDesktopField } = useMatchMediaField()
    const [open, setOpen] = useState(true);


    const visSmile = (smile) =>{
        if (Emoji.block && Game.room){
            socket.emit('sendEmoji_server', Game.roleInGame, Game.room, smile)
            Emoji.setBlock(false)
            setTimeout(() => {
                Emoji.setBlock(true)
            }, 5000);
            clearTimeout()
        }
    }

    useEffect(() =>{
        if (isMobileField){
            setOpen(true)
        }
    }, [isMobileField])

    const show = {
        opacity: 1,
        width: 320,
        scale: [0, 0.2, 0.4, 0.6, 0.8, 1],
        transition: {
            width: {
              duration: 0.4,
            },
            opacity: {
              duration: 0.25,
            },
            scale:{
                duration: 0.35
            }
        },
    };

    const hide = {
        width: 0,
        opacity: 0, 
        scale: [1, 0.8, 0.6, 0.4, 0.2, 0],
        transition: {
            width: {
              duration: 0.4,
            },
        },
    };

    return ( 
        <div className="emotions-main">
            {isDesktopField &&
            <AnimatePresence initial={false}>
            {ViewEmoji.isVisible &&
                <motion.div 
                className="box"
                initial={{
                    width: 0,
                    opacity: 0,
                }}
                animate={show} 
                exit={hide}
                >
                    <div className="emotions-container">
                        <h1>Реакции</h1>
                        <div className="emoji-panel">
                            <div className="emoji-row">
                                <img src={angry} alt="" onClick={() => visSmile(angry)}/>
                                <img src={cool} alt="" onClick={() => visSmile(cool)}/>
                            </div>
                            <div className="emoji-row">
                                <img src={cry} alt="" onClick={() => visSmile(cry)}/>
                                <img src={devil} alt="" onClick={() => visSmile(devil)}/>
                            </div>
                            <div className="emoji-row">
                                <img src={happy} alt="" onClick={() => visSmile(happy)}/>
                                <img src={laughAndCry} alt="" onClick={() => visSmile(laughAndCry)}/>
                            </div>
                        </div>
                    </div>
                </motion.div >
            }
            </AnimatePresence>}
            {isDesktopField &&
            <div className="view">
                {ViewEmoji.isVisible?
                    <img src={arrowBack} alt="" onClick={() => ViewEmoji.setIsVisible(!ViewEmoji.isVisible)}/>
                    :
                    <img src={arrowRight} alt="" onClick={() => ViewEmoji.setIsVisible(!ViewEmoji.isVisible)}/>
                }
            </div>}
            {(isMobileField || isTabletField) &&
                        <div className="emotions-visible">
                            <div className="btn-emotion">
                                Реакции
                                {isTabletField &&
                                <button 
                                className={open ? 'active-emotion' : 'disable-emotion'} 
                                onClick={() => setOpen(!open)}
                                >
                                <span />
                                <span />
                                <span />
                                </button>}
                                {isMobileField &&
                                <button 
                                className={!open ? 'active-emotion' : 'disable-emotion'} 
                                onClick={() => { if (ViewMobileChatAndEmoji.blockbtnEmotion) {setOpen(!open); ViewMobileChatAndEmoji.setBlockbtnChat(!ViewMobileChatAndEmoji.blockbtnChat)}}}
                                >
                                <span />
                                <span />
                                <span />
                                </button>}
                            </div>
                            
                            
                            <div className="emotions-container" style={open ? {boxShadow: 'none' } : {transform: 'translateY(-100%)'}}>
                                <h1>Реакции</h1>
                                <div className="emoji-panel">
                                    <div className="emoji-row">
                                        <img src={angry} alt="" onClick={() => visSmile(angry)}/>
                                        <img src={cool} alt="" onClick={() => visSmile(cool)}/>
                                    </div>
                                    <div className="emoji-row">
                                        <img src={cry} alt="" onClick={() => visSmile(cry)}/>
                                        <img src={devil} alt="" onClick={() => visSmile(devil)}/>
                                    </div>
                                    <div className="emoji-row">
                                        <img src={happy} alt="" onClick={() => visSmile(happy)}/>
                                        <img src={laughAndCry} alt="" onClick={() => visSmile(laughAndCry)}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        }
        </div>
    );
}
 
export default observer(Emotions);