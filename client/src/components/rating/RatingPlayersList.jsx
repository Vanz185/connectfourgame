import React from "react";

const RatingPlayersList = ({rating}) => {
    return ( 
        <>
            {rating.length !== 0 &&
                rating.map((row, index) =>
                    <ul className='player-rating' key={index}>
                        <li className='name-rating'>{row.surName+' '+row.firstName+' '+row.secondName}</li>
                        <li className='coutGames-rating'>{row.total_cnt}</li>
                        <li className='coutWin-rating'>{row.wins_cnt}</li>
                        <li className='coutLos-rating'>{row.loses_cnt}</li>
                        <li className='procWin-rating'>{Math.trunc(row.wins_prc*100)+'%'}</li>
                    </ul>
            )}
        </>
    );
}
 
export default RatingPlayersList;