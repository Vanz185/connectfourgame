import React from "react";
import MyButton from '../../UI/button/MyButton'
import blockUser from '../../UI/icons/blockUser.svg'
import unlockUser from '../../UI/icons/unlockUser.svg'
import editUser from '../../UI/icons/editUser.svg'

const PlayersList = ({playersList,editActive, setModal, setPlayer}) => {

    const monthNames = [
      "января", "февраля", "марта", "aпреля", "мая", "июня",
      "июля", "августа", "сентября", "октября", "ноября", "декабря"
    ]

    

    return ( 
        <>
            {playersList.map((playerList, index) =>
                <ul className='playerList' key={index}>
                  <li className='name-playerList'>{playerList.surName +' '+ playerList.firstName +' '+ playerList.secondName}</li>
                  <li className='age-playerList'>{playerList.age}</li>
                  <li className='gender-playerList'>{playerList.gender === true ?'М':'Ж'}</li>
                  <li className={playerList.statusActive ? 'status-playerList statAct' : 'status-playerList statNoAct'}>
                    {playerList.statusActive ? 'Активен' : 'Заблокирован'}
                  </li>
                  <li className='dateCreate-playerList'>{new Date(playerList.createdAt).getDate()+' '+monthNames[(new Date(playerList.createdAt).getMonth())] + ' '+new Date(playerList.createdAt).getFullYear()}</li>
                  <li className='dateEdit-playerList'>{new Date(playerList.updatedAt).getDate()+' '+monthNames[(new Date(playerList.updatedAt).getMonth())] + ' '+new Date(playerList.updatedAt).getFullYear()}</li>
                  <li className="buttons-playerList">
                    <MyButton onClick={() => editActive(index, !playerList.statusActive, playerList.id)}><img src={playerList.statusActive ? blockUser:unlockUser} alt="" /></MyButton>
                    <MyButton onClick={() => {setPlayer({playerList:playerList, index: index}); setModal(true)}}><img src={editUser} alt="" /></MyButton>
                  </li>
                </ul>
            )}
        </>
    );
}
 
export default PlayersList;