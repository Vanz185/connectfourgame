import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import TimerField from '../../store/TimerField';
import Game from '../../store/Game';

const Timer = () => {
    const strSec = String(Math.floor((TimerField.time / 1000) % 60));
    const strMin = String(Math.floor((TimerField.time / 1000 / 60) % 60));
    
    useEffect(() => {
        if(Game.room) {
            const timeint = setInterval(() => { TimerField.setTime() }, 1000);
        
        if (Game.gameOver) return clearInterval(timeint);
        return () => {
            clearInterval(timeint);
        }
        }
    }, [Game.gameOver, TimerField]);


    return ( 
        <div id="game-time">
                {("0" + strMin).slice(-2)}
                :
                {("0" + strSec).slice(-2)}
        </div>
    );
}
 
export default observer(Timer);