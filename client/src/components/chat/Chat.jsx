import React, { useContext, useEffect, useRef, useState } from 'react';
import { motion, AnimatePresence} from "framer-motion";
import MyInput from '../../UI/input/MyInput';
import ListMessage from './ListMessage';
import { observer } from 'mobx-react-lite';
import sendBtn from '../../UI/icons/send.svg'
import MyButton from '../../UI/button/MyButton';
import './ChatStyle.scss'
import './ChatAdaptive.scss'
import ChatStore from '../../store/ChatStore';
import UserStore from '../../store/UserStore';
import Game from '../../store/Game';
import ViewChat from '../../store/ViewChat';
import arrowBack from '../../UI/icons/arrowBack.svg'
import arrowRight from '../../UI/icons/arrowRight.svg'
import { useMatchMediaField } from '../../hooks';
import ViewMobileChatAndEmoji from '../../store/ViewMobileChatAndEmoji';

const Chat = () => {
    const socket = UserStore.socket
    const divRef = useRef(null)
    const [open, setOpen] = useState(true);
    const { isMobileField, isTabletField, isDesktopField } = useMatchMediaField()
    const [message, setMessage] = useState('');
    const scrollView = () => {
        if (ViewChat.isVisible){
            divRef.current.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start'});
        }
    };
  
    useEffect(() => {
        scrollView()
    }, [ChatStore.listMessage, ViewChat.isVisible]);

    useEffect(() => {
        socket.on('chat', (payload) => {
            ChatStore.setListMessage(payload) 
        });
    
    }, []);

    useEffect(() =>{
        if (isMobileField){
            setOpen(true)
        }
    }, [isMobileField])

    const show = {
        opacity: 1,
        width: 320,
        scale: [0, 0.2, 0.4, 0.6, 0.8, 1],
        transition: {
            width: {
              duration: 0.4,
            },
            opacity: {
              duration: 0.25,
            },
            scale:{
                duration: 0.35
            }
        },
    };

    const hide = {
        width: 0,
        opacity: 0, 
        scale: [1, 0.8, 0.6, 0.4, 0.2, 0],
        transition: {
            width: {
              duration: 0.4,
            },
        },
    };


    
    
    const addMessage = () =>{
        const time = new Date().toISOString()
        const userId = UserStore.user.id
        const gameRoom = Game.room
        const roleInGame = Game.roleInGame
        if (message && Game.room) {
            const payload = { 
                message, 
                gameRoom, 
                time,
                userId,
                roleInGame,
            };
            
            socket.emit('message', payload);
            setMessage('')
        }
    }
    
    return ( 

        <>
        <div className="chat-main">
            {isDesktopField &&
            <div className="view-chat">
                {ViewChat.isVisible?
                    <img src={arrowRight} alt="" onClick={() => ViewChat.setIsVisible(!ViewChat.isVisible)}/>
                    :
                    <img src={arrowBack} alt="" onClick={() => ViewChat.setIsVisible(!ViewChat.isVisible)}/>
                }
            </div>}
            {isDesktopField &&
            <AnimatePresence initial={false}>
                {ViewChat.isVisible &&
                    <motion.div 
                    className="box"
                    initial={{
                        width: 0,
                        opacity: 0,
                    }}
                    
                    animate={show} 
                    exit={hide}
                    
                    >
                        <div className="chat-container">
                            <div className="msgs-panel">
                                <div className='msgs-container'>
                                    <ListMessage/>
                                    <div className='scroll_dawn' ref={divRef}></div>
                                </div>
                            </div>
                            <div className="msg-interactive-elements">
                                <MyInput
                                    value={message}
                                    onChange={e => setMessage(e.target.value)}
                                    placeholder="Сообщение..."
                                    type="text" />
                                <MyButton onClick={addMessage}><img src={sendBtn} alt='' /></MyButton>
                            </div>
                        </div>
                    </motion.div >
                }
            </AnimatePresence>}
            {(isMobileField || isTabletField) &&
                <div className='chat-visible'>
                    <div className="btn-chat">
                        Чат
                        {isTabletField &&
                        <button className={open ? 'active-emotion' : 'disable-emotion'} 
                        onClick={() => setOpen(!open)}
                        >
                        <span />
                        <span />
                        <span />
                        </button>}
                        {isMobileField &&
                        <button className={!open ? 'active-emotion' : 'disable-emotion'} 
                        onClick={() => { if (ViewMobileChatAndEmoji.blockbtnChat) {setOpen(!open); ViewMobileChatAndEmoji.setBlockbtnEmotion(!ViewMobileChatAndEmoji.blockbtnEmotion)}}}
                        >
                        <span />
                        <span />
                        <span />
                        </button>}


                    </div>
                    <div className="chat-container" style={open ? {boxShadow: 'none' } : {transform: 'translateY(-100%)'}}>
                        <div className="msgs-panel">
                            <div className='msgs-container'>
                                <ListMessage/>
                                <div className='scroll_dawn' ref={divRef}></div>
                            </div>
                        </div>
                        <div className="msg-interactive-elements">
                            <MyInput
                                value={message}
                                onChange={e => setMessage(e.target.value)}
                                placeholder="Сообщение..."
                                type="text" />
                            <MyButton onClick={addMessage}><img src={sendBtn} alt='' /></MyButton>
                        </div>
                    </div>
                </div>
                
            }

        </div>
        
        </>

    );
}
 
export default observer(Chat);