import { observer } from 'mobx-react-lite';
import React, { useContext } from 'react';
import ChatStore from '../../store/ChatStore';
import UserStore from '../../store/UserStore';


const ListMessage = () => {
    
    return ( 
        <>
        {ChatStore.listMessage.length === 0?
            <div className='noMessage'> Сообщений нет</div>
        :
        ChatStore.listMessage.map((mes, index) =>
            <div className={mes.idUser === UserStore.user.id ? "msg-container me" : "msg-container other"} key={index}>
                <div className="msg-header">
                    <div className={mes.role === 'x' ? "subject-name x" : "subject-name zero"}>{mes.firstName+' '+mes.surName}</div>
                    <div className="time">{new Date(mes.createdMesAt).getHours()+':'+ String(new Date(mes.createdMesAt).getMinutes()).padStart(2, '0')}</div>
                </div>
                <div className="msg-body">{mes.message}</div>
            </div>
        )}
        </>

    );
}
 
export default observer(ListMessage);