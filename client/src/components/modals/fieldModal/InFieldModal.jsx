import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import './InFieldModalStyle.scss'
import './InFieldModalAdaptive.scss'
import blueToken from '../../../UI/icons/blueToken.svg'
import orangeToken from '../../../UI/icons/orangeToken.svg'
import exitModal from '../../../UI/icons/exitModal.svg'
const InFieldModal = ({dataField, setVisible}) => {
    
    const styleBack = (i,j) => {
        if (dataField.gameOver !== 'end') {                      
            if (dataField.gameOver[0].filter(mas => JSON.stringify(mas) === JSON.stringify([i,j])).length !== 0) {     
                if (dataField.gameOver[1][0] === 'x')        
                    return 'winSquareX';
                else
                    return 'winSquareO';
            }
          }
        return 'cell';
    };

    const exit = () =>{
        setVisible(false)
    }
    
    return ( 
        <>
        
        <div className="field-modal" >
            <div className='exit'>
                <img src={exitModal} alt="" onClick={() => exit()}/>
            </div>
            
            <div className='field'>
            {dataField.field.length !==0 ? 
            dataField.field.map((row, i) => (
                row.map((ch, j) =>
                    <div key={j} className={`${styleBack(i, j)}`}>
                        {ch &&
                            <img src={ch ==='x'? blueToken: orangeToken} alt="" width='100%' height='100%'/>
                        }
                    </div>
                ) 
            ))
            : 'Поле не удалось получить. Попробуйте позже...'}
            </div>
        </div>
        
        </>
    );
}
 
export default observer(InFieldModal);