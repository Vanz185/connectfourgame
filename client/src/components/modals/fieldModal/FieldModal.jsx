import React, { useContext, useEffect } from 'react';
import style from './FieldModal.module.css'
import { observer } from 'mobx-react-lite';

const FieldModal = ({visible,setVisible, children}) => {
    const rootClasses = [style.createPlayer]

    
    if (visible) {
        rootClasses.push(style.active);
    }

    return (  

        <div className={rootClasses.join(' ')} onClick={() => setVisible(false)}>
            <div className={style.createPanel} onClick={(e) => e.stopPropagation()}>
                {children}
            </div>
        </div>

    );
}
 
export default observer(FieldModal);