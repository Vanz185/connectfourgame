import React, { useContext } from 'react';
import winIcon from '../../../UI/icons/winIcon.svg'
import { observer } from 'mobx-react-lite';
import Game from '../../../store/Game';
import MyButton from '../../../UI/button/MyButton';
import './InWinModalStyle.scss'

const InWinModal = ({startNew, outGame}) => {
    
    
    return ( 
        <>
        {Game.gameOver !== 'end' && Game.gameOver !==false &&
			<div className='modal-container'>
                <img src={winIcon} alt="" />
                <p className='name-win'>
                    Иванов Иван Иванович Победил</p>
                <div className='btn-modalWin'>
                    <MyButton className='btn-goNewGame' onClick={startNew}>Новая игра</MyButton>
                    <MyButton className='btn-goMenu' onClick={outGame}>Выйти в меню</MyButton>
                </div>
			</div>}
		{Game.gameOver === 'end' && Game.gameOver !==false &&
            <div className='modal-container'>
                <img src={winIcon} alt="" />
                <p className='name-win'>Ничья</p>
                <div className='btn-modalWin'>
                    <MyButton className='btn-goNewGame' onClick={startNew} >Новая игра</MyButton>
                    <MyButton className='btn-goMenu' onClick={outGame}>Выйти в меню</MyButton>
                </div>
            </div>}
        
        </>
    );
}
 
export default observer(InWinModal);