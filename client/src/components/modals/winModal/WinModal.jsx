import React, { useContext, useEffect } from 'react';
import style from './WinModal.module.css'
import { observer } from 'mobx-react-lite';
import { AuthContext } from '../../../App';
import Game from '../../../store/Game';

const WinModal = ({visible, setVisible, children}) => {
    const rootClasses = [style.createPlayer]

    
    useEffect(()=>{
        if (Game.gameOver !== false && Game.roleInGame !== false){
            setVisible(true)
        } else{
            setVisible(false)
        }
    }, [Game.gameOver])
    
    if (visible) {
        rootClasses.push(style.active);
    }

    return (  

        <div className={rootClasses.join(' ')}>
            <div className={style.createPanel} onClick={(e) => e.stopPropagation()}>
                {children}
            </div>
        </div>

    );
}
 
export default observer(WinModal);