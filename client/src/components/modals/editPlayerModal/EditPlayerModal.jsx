import React, { useContext, useEffect } from 'react';
import style from './EditPlayerModal.module.css'
import { observer } from 'mobx-react-lite';

const EditPlayerModal = ({visible, setVisible,setPlayer, children}) => {
    const rootClasses = [style.editPlayer]

    
    if (visible) {
        rootClasses.push(style.active);
    }

    return (  

        <div className={rootClasses.join(' ')} onClick={() => {setVisible(false); setPlayer(false)}}>
            <div className={style.editPanel} onClick={(e) => e.stopPropagation()}>
                {children}
            </div>
        </div>

    );
}
 
export default observer(EditPlayerModal);