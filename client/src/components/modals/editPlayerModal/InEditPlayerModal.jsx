import React, { useEffect, useState } from 'react';
import MyInput from '../../../UI/input/MyInput';
import MyButton from '../../../UI/button/MyButton';
import exitModal from '../../../UI/icons/exitModal.svg'
import man from '../../../UI/icons/man.svg'
import women from '../../../UI/icons/women.svg'
import './InEditPlayerModalStyle.scss'

const InEditPlayerModal = ({editPlayer, exit, player}) => {
    
    const [dataPlayer, setDataPlayer] = useState({fio: player.playerList.firstName +' '+ player.playerList.surName +' '+ player.playerList.secondName, age: player.playerList.age, gender: player.playerList.gender})
    const [disableBtn, setDisableBtn] = useState(false)

    useEffect(() =>{
        if (dataPlayer.fio.trim().split(/\s+/).length === 3 && dataPlayer.age !=='' && dataPlayer.gender !=='') setDisableBtn(false)
        else setDisableBtn(true)
    }, [dataPlayer]);

    return (  
        <div className='editPlayer-main'>
            <div className="exit-panel">
                <button onClick={() =>exit()}><img src={exitModal} alt=""/></button>
            </div>
            <h1>Измените игрока</h1>
            <div className="fio-set">
                <p>ФИО</p>
                <MyInput 
                    value={dataPlayer.fio}
                    onChange={e => setDataPlayer({...dataPlayer, fio: e.target.value})}
                    type="text" 
                    placeholder="Иванов Иван Иванович"
                />
            </div>
            <div className="age-gender-set">
                <div className="age-set">
                    <p>Возраст</p>
                    <MyInput 
                        value={dataPlayer.age}
                        onChange={e => setDataPlayer({...dataPlayer, age: e.target.value})}
                        type="text" 
                        placeholder="0"
                    />
                </div>
                <div className="gender-set">
                    <p>Пол</p>
                    <div className="gender">
                        <label className='women'>
                            <input
                                type="radio"
                                name="gender"
                                value="women"
                                id="women"
                                checked={dataPlayer.gender === false? true: false}
                                onChange={e => setDataPlayer({...dataPlayer, gender: false})}
                            />
                            <img src={women} alt="" />
                        </label>
                        <label className='man'>
                            <input
                                type="radio"
                                name="gender"
                                value="man"
                                id="man"
                                checked={dataPlayer.gender === true? true: false}
                                onChange={e => setDataPlayer({...dataPlayer, gender: true})}
                            />
                            <img src={man} alt="" />
                        </label>
                    </div>
                </div>
            </div>
            <div className="editBtn-player">
                <MyButton 
                    disabled = {disableBtn}
                    onClick={()=> editPlayer({dataPlayer:dataPlayer, index: player.index, id:player.playerList.id})}
                >
                    Изменить
                </MyButton>
            </div>
        </div>
    );
}
 
export default InEditPlayerModal;