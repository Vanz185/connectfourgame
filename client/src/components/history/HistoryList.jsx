import React from "react";
import VS from '../../UI/icons/VS.svg'
import MyButton from '../../UI/button/MyButton'
import fieldIcon from '../../UI/icons/fieldIcon.svg'
import UserStore from "../../store/UserStore";

const HistoryList = ({playersHistory, setDataField, setModal}) => {
    
    const monthNames = [
        "января", "февраля", "марта", "aпреля", "мая", "июня",
        "июля", "августа", "сентября", "октября", "ноября", "декабря"
    ]

    const checkField = (winField) => {
        setDataField(winField.field && winField.gameOver ?{field: winField.field, gameOver: winField.gameOver}:
            {field:[], gameOver:[]})
        setModal(true)
    }

    return ( 
        <>
        {playersHistory.length !== 0 &&
        playersHistory.map((playerHistory, index) =>
            <ul className={`players-history ${((playerHistory.gameResult === true && playerHistory.login1 === UserStore.user.login) || 
                (playerHistory.gameResult === false && playerHistory.login2 === UserStore.user.login))? 'back-green' : 'back-red'}`} key={index}>
                <li className='info-players-game'>
                <p>
                {playerHistory.surName2+' '+playerHistory.firstName2.slice(0,1)+'. '+playerHistory.secondName2.slice(0,1)+'.'}
                </p>
                <span><img src={VS} alt="" /></span>
                <p>
                {playerHistory.surName1+' '+playerHistory.firstName1.slice(0,1)+'. '+playerHistory.secondName1.slice(0,1)+'.'}
                </p>
                </li>
                <li className='date-game'>{new Date(playerHistory.gameEnd).getDate()+' '+monthNames[new Date(playerHistory.gameEnd).getMonth()]+' '+new Date(playerHistory.gameEnd).getFullYear()}</li>
                <li className='time-game'>{('0'+Math.floor(((Date.parse(playerHistory.gameEnd)-Date.parse(playerHistory.gameBegin)) / 1000 / 60) % 60)).slice(-2)+' мин '+('0'+Math.floor(((Date.parse(playerHistory.gameEnd)-Date.parse(playerHistory.gameBegin)) / 1000) % 60)).slice(-2)+' сек'}</li>
                <li className='game-field'>
                    <MyButton onClick={() => checkField(playerHistory.winField)}><img src={fieldIcon} alt="" /></MyButton>
                </li>
            </ul>
        )}
        
        </>
    );
}
 
export default HistoryList;