import React from 'react';
import classes from './Loading.module.css'
import { RotatingLines } from 'react-loader-spinner'

const Loading = () => {
    return (  
        <div className={classes.loading}>
            <RotatingLines
                strokeColor="#43cbfe"
            />
        </div>
        
        
    );
}
 
export default Loading;