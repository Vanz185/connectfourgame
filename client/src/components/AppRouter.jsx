import React, { useContext } from 'react';
import {Route, Routes, Navigate} from 'react-router-dom';
import {adminRoutes, publicRoutes, authRoutes} from '../router'
import { AuthContext } from '../App';
import UserStore from '../store/UserStore';
import { observer } from 'mobx-react-lite'
import Header from '../UI/header/Header';
import NavBar from '../UI/navBar/NavBar';

const AppRouter = () => {
    
    return (  
      UserStore.isAuth
      ?
      <Routes>
        <Route path='/' element={<NavBar/>}>
          <Route path='/' element={<Header/>}>  
            {UserStore.isAdmin
            ?
            adminRoutes.map(route =>
              <Route
                  element={route.element}
                  path={route.path}
                  exact={route.exact}
                  key={route.path}
              />
            )
            :
            authRoutes.map(route =>
              <Route
                  element={route.element}
                  path={route.path}
                  exact={route.exact}
                  key={route.path}
              />
            )
            }
            <Route path='*'element={<Navigate to='/activePlayers'/>}/>
          </Route>
        </Route>
      </Routes>
      :
      <Routes>
        {publicRoutes.map(route =>
          <Route
              element={route.element}
              path={route.path}
              exact={route.exact}
              key={route.path}
          />
        )}
        <Route path='*'element={<Navigate to='/login'/>}/>
      </Routes>
      
        
    );
}
 
export default observer(AppRouter);