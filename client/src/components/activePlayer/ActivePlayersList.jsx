import React from "react";
import MyButton from "../../UI/button/MyButton";
import inviteIcon from '../../UI/icons/inviteIcon.svg'

const ActivePlayersList = ({statusPlayers, inviteUser}) => {
    const monthNames = [
        "января", "февраля", "марта", "aпреля", "мая", "июня",
        "июля", "августа", "сентября", "октября", "ноября", "декабря"
    ]

    const chooseOponent =(oponent) =>{
        inviteUser(oponent);
    }
    
    return ( 
        <>
        {statusPlayers.length !==0 &&
        statusPlayers.map((statusPlayer, index) =>
            <ul className="player-active" key={index}>
                <li className='name-active'>{statusPlayer.surName +' '+ statusPlayer.firstName +' '+ statusPlayer.secondName}</li>
                <li className='lastgame-active'>
                    {statusPlayer.lastGame
                    ?
                    new Date(statusPlayer.lastGame).getDate()+' '+monthNames[(new Date(statusPlayer.lastGame).getMonth())] + ' '+new Date(statusPlayer.lastGame).getFullYear() + ' '+("0"+new Date(statusPlayer.lastGame).getHours()).slice(-2)+':'+("0"+new Date(statusPlayer.lastGame).getMinutes()).slice(-2)
                    :
                    'Еще не было игр'
                    }
                </li>
                <li className='status-active'>
                    {statusPlayer.status === 0 
                    ?
                    <p className='p-free'> Свободен</p>
                    :
                    <p className='p-play'> В игре</p>
                    }
                </li>
                <li className="btn-active">
                <MyButton disabled = {statusPlayer.status !== 0} onClick={()=> chooseOponent(statusPlayer)}><img src={inviteIcon} alt="" /></MyButton>    
                </li>
            </ul>
        )}
        </>
    );
}
 
export default ActivePlayersList;