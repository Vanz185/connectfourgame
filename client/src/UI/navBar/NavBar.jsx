import { React, useContext, useEffect } from 'react';
import { NavLink, Outlet, useNavigate } from 'react-router-dom';
import { observer } from 'mobx-react-lite'
import './NavBarStyle.scss'
import UserStore from '../../store/UserStore';
import { useMatchMedia } from '../../hooks';



const NavBar = () => {
    const { isMobile, isTablet, isDesktop } = useMatchMedia();

    return ( 
        <>
            {isDesktop &&
            <footer>
                <div className="nav-panel">
                    <div><NavLink to='/gameField' className='nav-item'>Игровое поле</NavLink></div>
                    <div><NavLink to='/ratingField' className='nav-item'>Рейтинг</NavLink></div>
                    <div><NavLink to='/activePlayers' className='nav-item'>Активные игроки</NavLink></div>
                    <div><NavLink to='/gameHistory' className='nav-item'>История игр</NavLink></div>
                    {UserStore.isAdmin && <div><NavLink to='/listOfPlayers' className='nav-item'>Список игроков</NavLink></div>}
                </div>
            </footer>
            }
            <Outlet />
        </>
    );
}
 
export default observer(NavBar);