import React, { useContext, useEffect, useState } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import { ContextAuth } from '../../App';
import './BurgerStyle.scss';
import UserStore from '../../store/UserStore';
import logoHeader from '../../UI/icons/logoHeader.svg'
import burgerExit from '../../UI/icons/burgerExit.svg'

const BurgerMenu = ({exit}) => {
  const [open, setOpen] = useState(false);
  
  useEffect(()=>{
    setOpen(false)
}, [useLocation()])

  return (
    <>
      <div className='header'>
        <div className='logo-header'>
          <img src={logoHeader} alt=""/>
        </div>
        <button className={open ? 'active-burg' : 'btn-burger'} onClick={() => setOpen(!open)}>
          <span />
          <span />
          <span />
        </button>
      </div>
      <div className='content-burger' style={open ? {transform: 'translateY(0)'} : {boxShadow: 'none' }}>
        <ul className='navigate-header'>
          <li><NavLink to='/gameField' className='nav-item'>Игровое поле</NavLink></li>
          <li><NavLink to='/ratingField' className='nav-item'>Рейтинг</NavLink></li>
          <li><NavLink to='/activePlayers' className='nav-item'>Активные игроки</NavLink></li>
          <li><NavLink to='/gameHistory' className='nav-item'>История игр</NavLink></li>
          {UserStore.isAdmin && <li><NavLink to='/listOfPlayers' className='nav-item'>Список игроков</NavLink></li>}
          <li><button className='nav-item logo-out' onClick={exit}>
            Выйти
            <img src={burgerExit} alt="" />
          </button></li>
        </ul>
      </div>
    </>
  );
};

export default BurgerMenu;