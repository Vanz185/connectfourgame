import { React, useEffect} from 'react';
import { Outlet, useNavigate } from 'react-router-dom';

import './Header.scss'
import exitIcon from '../icons/exit.svg'
import logoHeader from '../icons/logoHeader.svg'

import Game from '../../store/Game';
import { observer } from 'mobx-react-lite'
import UserStore from '../../store/UserStore';
import jwt_decode from 'jwt-decode';
import ChatStore from '../../store/ChatStore';
import TimerField from '../../store/TimerField';

import Emoji from '../../store/Emoji';

import { useMatchMedia } from '../../hooks/useMatchMedia';
import GamePanel from '../../components/gameField/GamePanel';
import BurgerMenu from './BurgerMenu';

const Header = () => {
    
    const { isMobile, isTablet, isDesktop } = useMatchMedia();

    
    const socket = UserStore.socket
    const navigate = useNavigate()

    const exit = () =>{
        socket.emit('disconUsersOnline', UserStore.user.id);
        localStorage.clear();
        window.location.reload();
    }

    useEffect(()=>{
        
        socket.emit('check_last_game', UserStore.user.id, UserStore.user.login)

        socket.on('start_game', () => {
            navigate('/gameField');
        });

        

        socket.on('reconnecting', (indexRoom, dataLastGame)=>{
            Game.setPlayersRoom(indexRoom.players)
            Game.setField(indexRoom.field)
            Game.setMode(indexRoom.mode)
            Game.setWinComb(false)
            Game.setRoleInGame(dataLastGame.roleInGame)
            Game.setRoom(indexRoom)
            TimerField.setStartGame(Date.parse(dataLastGame.gameBegin))
            navigate('/gameField');
        })

        socket.on('loadLastGame',(playersArr, time, winField, messages) =>{
            Game.setPlayersRoom(playersArr)
            Game.setField(winField.field)
            Game.setWinComb(winField.gameOver)
            TimerField.setTimeEndGame(time)
            ChatStore.setListMessage(messages)
        })

        socket.on('kick helper', () => { 
            exit()
            alert('Вас заблокировали')
        })

        socket.on('sendEmoji_client', (role, smile) =>{
            if (role === 'x'){
                
                Emoji.setSmileX(smile)
                Emoji.setIsVisibleX(true)
                setTimeout(() => {
                    Emoji.setIsVisibleX(false)
                    Emoji.setSmileX(false)
                }, 2000);
            
                clearTimeout()
            } else {
                
                Emoji.setSmileO(smile)
                Emoji.setIsVisibleO(true)
                setTimeout(() => {
                    Emoji.setIsVisibleO(false)
                    Emoji.setSmileO(false)
                }, 2000);
            
                clearTimeout()
            }
        })

    },[])
    
    
    useEffect(()=>{
        if (!Game.room) {
            socket.removeAllListeners('requestInvite_client');
            socket.removeAllListeners('responseInvite_client');
            socket.removeAllListeners('join_room_client');
            socket.removeAllListeners('not_your_move');
            
            socket.on('not_your_move', () => {
                alert('Сейчас не ваш ход')
            });

            socket.on('requestInvite_client', (userReqIdSocket, portfolio_user_req) => {
                if (localStorage.getItem('token') !== null){
                    const token = localStorage.getItem('token')
                    const decoded = jwt_decode(token);
                    if (decoded.exp * 1000 < new Date().getTime()){
                        socket.emit('responseInvite_server', false, userReqIdSocket, UserStore.user.id, UserStore.user.login, portfolio_user_req);
                    } else {
                        const res = window.confirm(`Игрок ${portfolio_user_req[0].surName+' '+portfolio_user_req[0].firstName+' '+portfolio_user_req[0].secondName} приглашает вас в игру.\nПринять?`);
                        socket.emit('responseInvite_server', res, userReqIdSocket, UserStore.user.id, UserStore.user.login, portfolio_user_req);
                    }
                } else {
                    socket.emit('responseInvite_server', false, userReqIdSocket, UserStore.user.id, UserStore.user.login, portfolio_user_req);
                }
                
                
            });
    
            socket.on('responseInvite_client', ({res, userRes}) => {
                alert('Тебе отказали')
                
            });
            
            socket.on('join_room_client', (room, arrPlayers, role, begin) => {
                ChatStore.setListMessage(room.chat)
                Game.setPlayersRoom(arrPlayers)
                Game.setField(room.field)
                Game.setMode(room.mode)
                Game.setWinComb(false)
                Game.setRoleInGame(role)
                TimerField.setStartGame(Date.parse(begin))
                Game.setRoom(room)
                socket.emit('join_room_server', room.id, UserStore.user.id, UserStore.user.login)
                
            });
        }
    }, [Game.room])

    return (  
        <>
        
        <header>
        {isDesktop &&
            <>
            <div className="logo"><img src={logoHeader} alt=""/></div>
            <GamePanel/>
            <div className='exit-header'>
                <button onClick={exit}>
                    <img src={exitIcon} alt=''/>
                </button>
            </div>
            </>
        }
        {(isTablet || isMobile) &&
			<BurgerMenu exit={exit} />
        }
        </header>
        <Outlet />
        </>
    );
}
 
export default observer(Header);