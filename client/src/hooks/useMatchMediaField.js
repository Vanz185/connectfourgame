import { useState, useLayoutEffect } from 'react';

const queries = [
  '(max-width: 1024px)',
  '(min-width: 1025px) and (max-width: 1192px)',
  '(min-width: 1193px)'
];

export const useMatchMediaField = () => {
  const mediaQueryLists = queries.map((query) => matchMedia(query));

  const getValues = () => mediaQueryLists.map((list) => list.matches);

  const [values, setValues] = useState(getValues);

  useLayoutEffect(() => {
    const handler = () => setValues(getValues);

    mediaQueryLists.forEach((list) => list.addEventListener('change', handler));

    return () =>
      mediaQueryLists.forEach((list) =>
        list.removeEventListener('change', handler)
      );
  }, []);

  return ['isMobileField', 'isTabletField', 'isDesktopField'].reduce(
    (acc, screen, index) => ({
      ...acc,
      [screen]: values[index]
    }),
    {}
  );
};
